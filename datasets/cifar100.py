from torch.utils.data import DataLoader
from torchvision.datasets import CIFAR100
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision
import torch
import os


def cifar100_loader(args):
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],

                                     std=[0.229, 0.224, 0.225])

    transform_train = transforms.Compose([

        transforms.Resize((256, 256), interpolation=Image.BICUBIC),

        transforms.RandomCrop((224, 224)),

        transforms.RandomHorizontalFlip(),

        transforms.ToTensor(),

        normalize,

    ])

    train_dataset = torchvision.datasets.CIFAR100(root='./data', train=True, download=True,

                                                  transform=transform_train)

    if args.distributed:

        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)

    else:

        train_sampler = None

    train_loader = torch.utils.data.DataLoader(

        train_dataset, batch_size=args.batch_size, shuffle=(train_sampler is None),

        num_workers=args.workers, pin_memory=True, sampler=train_sampler)

    val_loader = torch.utils.data.DataLoader(

        datasets.CIFAR100(root='./data', train=False, download=True,

                          transform=transforms.Compose([

                              transforms.Resize((256, 256), interpolation=Image.BICUBIC),

                              transforms.CenterCrop((224, 224)),

                              transforms.ToTensor(),

                              normalize,

                          ])),

        batch_size=args.batch_size, shuffle=False,

        num_workers=args.workers, pin_memory=True)


def cifar10_loader(args):
    normalize = transforms.Normalize(mean=[0.491, 0.482, 0.447], std=[0.247, 0.243, 0.262])

    transform_train = transforms.Compose([

        transforms.RandomCrop(32, padding=4),

        transforms.RandomHorizontalFlip(),

        transforms.ToTensor(),

        normalize,

    ])

    train_dataset = torchvision.datasets.CIFAR10(root='../data', train=True, download=True,

                                                 transform=transform_train)

    train_loader = torch.utils.data.DataLoader(

        train_dataset, batch_size=args.batch_size, shuffle=True,

        num_workers=args.workers, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(

        datasets.CIFAR10(root='../data', train=False, download=True,

                         transform=transforms.Compose([transforms.ToTensor(),

                                                       normalize,

                                                       ])),

        batch_size=args.batch_size, shuffle=False,

        num_workers=args.workers, pin_memory=True)
    return train_loader, val_loader


def imagenet_loader(args):
    traindir = os.path.join(args.data, 'train')
    valdir = os.path.join(args.data, 'val')
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    if 'inception' in args.arch:
        #  crop_size, short_size = 299, 342
        crop_size, short_size = 299, 342
    else:
        crop_size, short_size = 224, 256
    train_dataset = datasets.ImageFolder(
        traindir,
        transforms.Compose([
            transforms.RandomResizedCrop(crop_size),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ]))

    if args.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    else:
        train_sampler = None

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=(train_sampler is None),
        num_workers=args.workers, pin_memory=True, sampler=train_sampler)

    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(valdir, transforms.Compose([
            transforms.Resize(short_size),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)
    return train_loader, val_loader


class Lighting(object):
    """Lighting noise(AlexNet - style PCA - based noise)"""

    def __init__(self, alphastd, eigval, eigvec):
        self.alphastd = alphastd
        self.eigval = eigval
        self.eigvec = eigvec

    def __call__(self, img):
        if self.alphastd == 0:
            return img

        alpha = img.new().resize_(3).normal_(0, self.alphastd)
        rgb = self.eigvec.type_as(img).clone() \
            .mul(alpha.view(1, 3).expand(3, 3)) \
            .mul(self.eigval.view(1, 3).expand(3, 3)) \
            .sum(1).squeeze()
        return img.add(rgb.view(3, 1, 1).expand_as(img))


from torchvision import models, transforms
from PIL import Image, ImageOps, ImageFilter


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    RED = "\033[1;31m"
    BLUE = "\033[1;34m"
    CYAN = "\033[1;36m"
    GREEN = "\033[0;32m"
    RESET = "\033[0;0m"
    BOLD = "\033[;1m"
    REVERSE = "\033[;7m"


normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])

__imagenet_pca = {
    'eigval': torch.Tensor([0.2175, 0.0188, 0.0045]),
    'eigvec': torch.Tensor([
        [-0.5675, 0.7192, 0.4009],
        [-0.5808, -0.0045, -0.8140],
        [-0.5836, -0.6948, 0.4203],
    ])
}


def fbresnet_augmentor(img_size=224):
    print('Using fbresnet augmentation')
    train_transform = transforms.Compose([
        transforms.RandomResizedCrop(img_size),
        transforms.ColorJitter(.4, .4, .4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        Lighting(0.1, __imagenet_pca['eigval'], __imagenet_pca['eigvec']),
        normalize,
    ])

    val_transform = transforms.Compose([
        transforms.Resize(256),  # shortest size to 256
        transforms.CenterCrop(img_size),
        transforms.ToTensor(),
        normalize,
    ])

    return train_transform, val_transform


def basic_augmentor(img_size=224):
    print('Using basic augmentation')
    train_transform = transforms.Compose([
        # transforms.Resize(256),
        transforms.RandomCrop(img_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normalize,
    ])

    val_transform = transforms.Compose([
        # transforms.Resize(256),
        transforms.CenterCrop(img_size),
        transforms.ToTensor(),
        normalize,
    ])

    return train_transform, val_transform


def pytorch_augmentor(img_size=224):
    train_transform = transforms.Compose([
        transforms.RandomResizedCrop(img_size, scale=(0.5, 1.0)),
        # transforms.RandomCrop(img_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normalize,
    ])

    val_transform = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(img_size),
        transforms.ToTensor(),
        normalize,
    ])

    return train_transform, val_transform


def cifar_transform_32():
    train_transform = transforms.Compose([
        transforms.Pad(4, padding_mode='reflect'),
        transforms.RandomHorizontalFlip(),
        transforms.RandomCrop(32),
        transforms.ToTensor(),
        transforms.Normalize((0.5071, 0.4865, 0.4409), (0.2673, 0.2564, 0.2762))
    ])
    val_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5071, 0.4865, 0.4409), (0.2673, 0.2564, 0.2762))
    ])
    return train_transform, val_transform


