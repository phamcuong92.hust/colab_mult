python main_mult.py -a quantres18_32_84_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 256 \
      --type_data CIFAR100 \
      --lr 0.1 --lra 0.002 --wd 1e-4 --gpu 0 --use_wandb




python main_mult.py -a quantres18_32_4_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 256 \
      --type_data CIFAR100 \
      --lr 0.1 --lra 0.002 --wd 1e-4 --gpu 0 --use_wandb

python main_mult.py -a quantres18_42_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 256 \
      --type_data CIFAR100 \
      --lr 0.1 --lra 0.002 --wd 1e-4 --gpu 0 --use_wandb


python main_mult.py -a quantres18_32_42_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 256 \
      --type_data ImageNet -p 500 \
      --lr 0.1 --lra 0.002 --wd 1e-4 --gpu 0 --use_wandb

python main_mult_fitnet.py \
      -t quantres18_32_42_mult -a quantres18_2_mult \
      --epochs 100 --step-epoch 10 -j 4 -b 64 \
      --type_data ImageNet -p 2000 \
      --ac ./checkpoints/quantres18_32_42_mult/model_best_ImageNet_69_38.pth.tar \
      --resume ./checkpoints/quantres18_2_mult/model_best_ImageNet_56.228.pth.tar \
      --lr 0.01 --lra 0.001 --lr_t 0.005 --wd 1e-4 --T 5 --alpha_kd 0.4 \
      --gpu 1 --data ../data/imagenet --use_wandb