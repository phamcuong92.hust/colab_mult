# single model
from .quantres18_single import *
# mult model
from .quantres18_mult import *


# single model mobilenetv2
from .quantmobiv2_single import *
#from .mobilenetv2_origin import *
from .quantmobi_single import *