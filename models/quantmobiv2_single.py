"""
MobileNetV2 implementation used in
<Knowledge Distillation via Route Constrained Optimization>
"""

import torch
import torch.nn as nn
import math
from typing import Callable, Any, Optional, List

__all__ = ['quantmobilenetv2_32_mult', 'quantmobilenetv2_4_mult', 'quantmobilenetv2_2_mult',
           'quantmobilenetv2_8_mult', ]
model_urls = {
    'mobilenet_v2': 'https://download.pytorch.org/models/mobilenet_v2-b0353104.pth',
}


BN = None

def _make_divisible(v: float, divisor: int, min_value: Optional[int] = None) -> int:
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    """
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v


def conv_bn(conv_func, act_func, inp, oup, bit, kernel_size=3, stride=1, groups=1, **kwargs):
    dilation = 1
    padding = (kernel_size - 1) // 2 * dilation
    return nn.Sequential(
        conv_func(inp, oup, bit, kernel_size=kernel_size, stride=stride, bias=False, padding=padding, groups=groups, **kwargs),
        nn.BatchNorm2d(oup),
        act_func(bit)
    )


def conv_1x1_bn(conv_func, act_func, inp, oup, bit, **kwargs):
    return nn.Sequential(
        conv_func(inp, oup, bit, kernel_size=1, stride=1, padding=0, bias=False, **kwargs),
        nn.BatchNorm2d(oup),
        act_func(bit)
    )


class InvertedResidual(nn.Module):
    def __init__(self, conv_func, act_func, inp, oup, bit, stride, expand_ratio, **kwargs):
        super(InvertedResidual, self).__init__()
        self.blockname = None

        self.stride = stride
        assert stride in [1, 2]
        hidden_dim = int(round(inp * expand_ratio))
        self.use_res_connect = self.stride == 1 and inp == oup

        layers: List[nn.Module] = []
        if expand_ratio != 1:
            # pw
            layers.append(conv_bn(conv_func, act_func, inp, hidden_dim, bit, kernel_size=1, **kwargs))
        layers.extend([
            # dw
            conv_bn(conv_func, act_func, hidden_dim, hidden_dim, bit, stride=stride, groups=hidden_dim, **kwargs),
            # pw-linear
            conv_func(hidden_dim, oup, bit=bit, kernel_size=1, stride=1, padding=0, bias=False, **kwargs),
            nn.BatchNorm2d(oup),
        ])
        self.conv = nn.Sequential(*layers)
        self.out_channels = oup
        self.names = ['0', '1', '2', '3', '4', '5', '6', '7']

    def forward(self, x):
        t = x
        if self.use_res_connect:
            return t + self.conv(x)
        else:
            return self.conv(x)


class MobileNetV2(nn.Module):
    """mobilenetV2"""
    def __init__(self, conv_func, act_func, bit,
                 num_classes=1000,
                 width_mult=1.
                 , **kwargs):
        super(MobileNetV2, self).__init__()
        input_channel = 32
        last_channel = 1280
        # setting of inverted residual blocks
        self.interverted_residual_setting = [
            # t, c, n, s
            [1, 16, 1, 1],
            [6, 24, 2, 2], #cifar 10 use this s=1 and cifar 100 use s = 2
            [6, 32, 3, 2],
            [6, 64, 4, 2],
            [6, 96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]

        # building first layer
       # assert input_size % 32 == 0
        round_nearest = 8
        input_channel = _make_divisible(input_channel * width_mult, round_nearest)
        self.last_channel = _make_divisible(last_channel * max(1.0, width_mult), round_nearest)

        self.conv1 = conv_bn(conv_func, act_func, inp=3, oup=input_channel, bit=32, stride=2, **kwargs)

        # building inverted residual blocks
        self.blocks = nn.ModuleList([])
        for t, c, n, s in self.interverted_residual_setting:
            output_channel = _make_divisible(c * width_mult, round_nearest)
            layers = []
            strides = [s] + [1] * (n - 1)
            for stride in strides:
                layers.append(InvertedResidual(conv_func, act_func, input_channel, output_channel, bit, stride, expand_ratio=t, **kwargs))
                input_channel = output_channel
            self.blocks.append(nn.Sequential(*layers))
           # self.blocks.append(nn.Sequential(*layers))
        # building last several layers
        self.conv2 = conv_bn(conv_func, act_func, input_channel, self.last_channel, bit, kernel_size=1, **kwargs)
        # make it nn.Sequential
      #  self.blocks = nn.Sequential(*blocks)
        # building classifier
        self.last_channel = int(1280 * width_mult) if width_mult > 1.0 else 1280
  #      self.conv2 = conv_1x1_bn(conv_func, act_func, input_channel, self.last_channel, bit, **kwargs)

        self.classifier = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(self.last_channel, num_classes),
        )
        #nn.Linear(self.last_channel, num_classes)
        self._initialize_weights()

    def get_bn_before_relu(self):
        bn1 = self.blocks[1][-1].conv[-1]
        bn2 = self.blocks[2][-1].conv[-1]
        bn3 = self.blocks[4][-1].conv[-1]
        bn4 = self.blocks[6][-1].conv[-1]
        return [bn1, bn2, bn3, bn4]

    def get_feat_modules(self):
        feat_m = nn.ModuleList([])
        feat_m.append(self.conv1)
        feat_m.append(self.blocks)
        return feat_m

    def forward(self, x, is_feat=False):

        out = self.conv1(x)
        f0 = out

        out = self.blocks[0](out)
        out = self.blocks[1](out)
        f1 = out
        out = self.blocks[2](out)
        f2 = out
        out = self.blocks[3](out)
        out = self.blocks[4](out)
        f3 = out
        out = self.blocks[5](out)
        out = self.blocks[6](out)
        f4 = out

        out = self.conv2(out)

        out = nn.functional.adaptive_avg_pool2d(out, (1, 1))
        out = out.view(out.size(0), -1)
        f5 = out
        out = self.classifier(out)

        if is_feat:
            return [f0, f1, f2, f3, f4, f5], out
        else:
            return out

    def extract_feature(self, x):

        x = self.conv1(x)

        feat1 = self.blocks[2][:-1](self.blocks[0:2](x))
        feat2 = self.blocks[4][:-1](self.blocks[3:4](feat1))
        feat3 = self.blocks[10][:-1](self.blocks[5:10](feat2))
        feat4 = self.blocks[12][:-1](self.blocks[11:12](feat3))

        out = self.blocks[12:](feat4)
        out = nn.functional.adaptive_avg_pool2d(out, (1, 1))
        out = torch.flatten(out, 1)
        out = self.classifier(out)

        return [feat1, feat2, feat3, feat4], out

    def get_channel_num(self):

        return [32, 24, 32, 96, 320]

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                n = m.weight.size(1)
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()

from modules_q import quant_mult as qm
def quantmobilenetv2_2_mult(num_classes=1000, **kwargs):
    bit = 2
    model = MobileNetV2(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                 width_mult=1.,
                **kwargs)
    return model

def quantmobilenetv2_32_mult(num_classes=1000, **kwargs):
    bit = 32
    model = MobileNetV2(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                 width_mult=1.,
                **kwargs)
    return model

def quantmobilenetv2_4_mult(num_classes=1000, **kwargs):
    bit = 4
    model = MobileNetV2(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                 width_mult=1.,
                **kwargs)
    return model

def quantmobilenetv2_8_mult(num_classes=1000, **kwargs):
    bit = 8
    model = MobileNetV2(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                 width_mult=1.,
                **kwargs)
    return model



if __name__ == '__main__':
    x = torch.randn(2, 3, 224, 224)

    net = quantmobilenetv2_32_mult(100)
    # out = net(x)
    # print(out.shape)
    # print(net)
    feats, out = net(x, is_feat=True)
  #  feats, logit = net(x, is_feat=True, preact=False)
    for f in feats:
        print(f.shape, f.min().item())
    # print(logit.shape)
    print(net.blocks)
    # for m in net.get_bn_before_relu():
    #     if isinstance(m, nn.BatchNorm2d):
    #         print('pass')
    #     else:
    #         print('warning')

