import torch
import torch.nn as nn
import math
from torch.nn.parameter import Parameter
import torch.nn.functional as F
from modules_q import quant_mult as qm
__all__ = [
    'quantres18_1_mult', 'quantres18_2_mult', 'quantres18_3_mult', 'quantres18_4_mult',
    'quantres18_6_mult',  'quantres18_8_mult',  'quantres18_32_mult'
]


class BasicBlock(nn.Module):
    expansion = 1
    num_layers = 2

    def __init__(self, conv_func, act_func, inplanes, planes, bit, stride=1,
                 downsample=None, bnaff=True, **kwargs):
        super(BasicBlock, self).__init__()
    #    self.bn3 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv1 = conv_func(inplanes, planes, bit, kernel_size=3, stride=stride,
                               padding=1, bias=False, **kwargs)
        self.act1 = act_func(bit)
        self.bn1 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv2 = conv_func(planes, planes, bit, kernel_size=3, stride=1,
                               padding=1, bias=False, **kwargs)
        self.bn2 = nn.BatchNorm2d(planes)
        self.act2 = act_func(bit)
        self.downsample = downsample

    def forward(self, x):
      #  print(x.shape)
     #   x = self.act1(x)
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.act1(out)
        out = self.conv2(out)
        out = self.bn2(out)
        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual

     #   out = self.bn3(out)

        out = self.act2(out)

        return out
        #
        # out = self.bn0(x)
        # if self.downsample is not None:
        #     residual = out
        # else:
        #     residual = x
        #
        # out = self.conv1(out)
        # out = self.bn1(out)
        # out = self.conv2(out)
        # out = self.bn2(out)
        #
        # if self.downsample is not None:
        #     residual = self.downsample(residual)
        #
        # out += residual
        #
        # return out


class Bottleneck(nn.Module):
    expansion = 4
    num_layers = 3

    def __init__(self, conv_func, inplanes, planes, archws, archas, stride=1,
                 downsample=None, bnaff=True, **kwargs):
        super(Bottleneck, self).__init__()
        assert len(archws) == 3
        assert len(archas) == 3
        self.bn0 = nn.BatchNorm2d(inplanes, affine=bnaff)
        self.conv1 = conv_func(inplanes, planes, archws[0], archas[0], kernel_size=1, bias=False, **kwargs)
        self.bn1 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv2 = conv_func(planes, planes, archws[1], archas[1], kernel_size=3, stride=stride,
                               padding=1, bias=False, **kwargs)
        self.bn2 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv3 = conv_func(
            planes, planes * 4, archws[2], archas[2], kernel_size=1, bias=False, **kwargs)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.downsample = downsample

    def forward(self, x):
        out = self.bn0(x)
        if self.downsample is not None:
            residual = out
        else:
            residual = x

        out = self.conv1(out)
        out = self.bn1(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(residual)

        out += residual

        return out


class ResNet(nn.Module):

    def __init__(self, block, conv_func, act_func, layers, bit, num_classes=1000,
                   bnaff=True, **kwargs):
        self.inplanes = 64
        self.conv_func = conv_func
        self.act_func = act_func
        super(ResNet, self).__init__()

        self.bit = bit

        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)

        self.bn1 = nn.BatchNorm2d(64)
        self.relu = act_func(2)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(
            block, conv_func, act_func, 64, 64, layers[0], bit, bnaff=bnaff, **kwargs)
        self.layer2 = self._make_layer(
            block, conv_func, act_func, 64, 128, layers[1], bit, stride=2, bnaff=bnaff, **kwargs)
        self.layer3 = self._make_layer(
            block, conv_func, act_func, 128, 256, layers[2], bit, stride=2, bnaff=bnaff, **kwargs)
        self.layer4 = self._make_layer(
            block, conv_func, act_func, 256, 512, layers[3], bit, stride=2, bnaff=bnaff, **kwargs)
        self.avgpool = nn.AvgPool2d(7)
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                if m.weight is not None:
                    m.weight.data.fill_(1)
                if m.bias is not None:
                    m.bias.data.zero_()
        self.reset_parameters(zero_init_residual=True)
    def _make_layer(self, block, conv_func, act_func, inplane, planes, blocks, bit, stride=1, bnaff=True, **kwargs):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv_func(inplane, planes * block.expansion, bit=bit, kernel_size=1,
                          stride=stride, bias=False, **kwargs),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(
            block(conv_func, act_func, inplane, planes, bit=bit, stride=stride, downsample=downsample, bnaff=bnaff, **kwargs))
        inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(conv_func, act_func, inplanes, planes, bit=bit, bnaff=bnaff, **kwargs))

        return nn.Sequential(*layers)

    def forward(self, x, extract=False):

        x = self.conv1(x)
        x = self.maxpool(x)
        x = self.bn1(x)
        x = self.relu(x)

        x =self.layer1(x)
        layer1 = x

        x = self.layer2(x)
        layer2 = x

        x =self.layer3(x)
        layer3 = x

        out = self.layer4(x)
      #  out = self.relu(out)
        layer4 = out
        assert out.shape[2] == 7
        assert out.shape[3] == 7
        out = self.avgpool(out)
        pool = out
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        if extract:
            return [layer1, layer2, layer3, layer4, pool], out
        else:
            return out

    def get_channel_num(self):
        return [64, 128, 256, 512, 512]
    def reset_parameters(self, zero_init_residual):

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

from torch.utils.model_zoo import load_url
model_urls = {
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}
def quantres18_2_mult(**kwargs):
    bits = 2

    model = ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)
   # print("load pretrain model")
  #  state_dict = load_url(model_urls["resnet18"], progress=True)
#    checkpoints = torch.load("/home/cuongpv/emips_kd/checkpoints/quantres18_864_mult/fitnet_KD_imagenetfull/teacher/model_best_ImageNet.pth.tar", map_location=lambda storage, loc: storage)
 #   state_dict = checkpoints["state_dict"]

  #  print(state_dict.keys())
  #  model.load_state_dict(state_dict, strict=False)
    return model

def quantres18_1_mult(**kwargs):
    bits = 1
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)

def quantres18_32_mult(**kwargs):
    bits = 32
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)

def quantres18_4_mult(**kwargs):
    bits = 4
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)

def quantres18_6_mult(**kwargs):
    bits = 6
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)

def quantres18_8_mult(**kwargs):
    bits = 8
    model = ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)
    print("load pretrain model")
    state_dict = load_url(model_urls["resnet18"], progress=True)
    #    checkpoints = torch.load("/home/cuongpv/emips_kd/checkpoints/quantres18_864_mult/fitnet_KD_imagenetfull/teacher/model_best_ImageNet.pth.tar", map_location=lambda storage, loc: storage)
    #   state_dict = checkpoints["state_dict"]

    #  print(state_dict.keys())
    model.load_state_dict(state_dict, strict=False)

  #  return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)
    return model

def quantres18_3_mult(**kwargs):
    bits = 3
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)



if __name__=="__main__":
    model = quantres18_2_mult()
    x = torch.randn(1, 3, 224, 224)
   # print(model)
    [b1, b2, b3, b4, pool], out = model(x, extract=True)
  #  transform = nn.Conv2d(64, 64, 1, bias=False)
   # b = transform(F.normalize(b1))
    print(b1.shape)
    print(b2.shape)
    print(b3.shape)
    print(b4.shape)
    print(pool.shape)
 #   print(model)