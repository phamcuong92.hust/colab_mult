'''MobileNet in PyTorch.
See the paper "MobileNets: Efficient Convolutional Neural Networks for Mobile Vision Applications"
for more details.
'''
import torch
import torch.nn as nn
import torch.nn.functional as F
from modules_q import quant_mult as qm

__all__ = ["quantmobilenet_2_mult", "quantmobilenet_4_mult", "quantmobilenet_8_mult", "quantmobilenet_32_mult"]
class MobileNet(nn.Module):
    def __init__(self, conv_func, act_func, bit, num_classes=1000, **kwargs):
        super(MobileNet, self).__init__()

        def conv_bn(conv_func, act_func, bit, inp, oup, stride, groups=1, **kwargs):
            return nn.Sequential(
                conv_func(inp, oup, bit, kernel_size=3, stride=stride, padding=1, groups=groups, bias=False, **kwargs),
                nn.BatchNorm2d(oup),
                act_func(bit)
            )

        def conv_dw(conv_func, act_func, bit, inp, oup, stride, **kwargs):
            return nn.Sequential(
                conv_func(inp, inp, bit, kernel_size=3, stride=stride, padding=1, groups=inp, bias=False, **kwargs),
                nn.BatchNorm2d(inp),
                act_func(bit),

                conv_func(inp, oup, bit, kernel_size=1, stride=1, padding=0, groups=1, bias=False, **kwargs),
                nn.BatchNorm2d(oup),
                act_func(bit)
            )

        self.model = nn.Sequential(
            conv_bn(conv_func, act_func, 32, 3, 32, 2, **kwargs),
            conv_dw(conv_func, act_func, bit, 32, 64, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 64, 128, 2, **kwargs),
            conv_dw(conv_func, act_func, bit, 128, 128, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 128, 256, 2, **kwargs),
            conv_dw(conv_func, act_func, bit, 256, 256, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 256, 512, 2, **kwargs),
            conv_dw(conv_func, act_func, bit, 512, 512, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 512, 512, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 512, 512, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 512, 512, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 512, 512, 1, **kwargs),
            conv_dw(conv_func, act_func, bit, 512, 1024, 2, **kwargs),
            conv_dw(conv_func, act_func, bit, 1024, 1024, 1, **kwargs),
            nn.AvgPool2d(7),
        )
        self.fc = nn.Linear(1024, num_classes)

    def forward(self, x):
        x = self.model(x)
        x = x.view(-1, 1024)
        x = self.fc(x)
        return x

    def get_bn_before_relu(self):
        bn1 = self.model[3][-2]
        bn2 = self.model[5][-2]
        bn3 = self.model[11][-2]
        bn4 = self.model[13][-2]

        return [bn1, bn2, bn3, bn4]

    def get_channel_num(self):

        return [128, 256, 512, 1024]

    def extract_feature(self, x):

        feat1 = self.model[3](self.model[0:3](x))
        feat2 = self.model[5](self.model[4:5](feat1))
        feat3 = self.model[11](self.model[6:11](feat2))
        feat4 = self.model[13](self.model[12:13](feat3))

        out = self.model[14](feat4)
        out = out.view(-1, 1024)
        out = self.fc(out)
        return [feat1, feat2, feat3, feat4], out


def quantmobilenet_32_mult(num_classes=1000, **kwargs):
    bit = 32
    model = MobileNet(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                **kwargs)
    return model

def quantmobilenet_8_mult(num_classes=1000, **kwargs):
    bit = 8
    model = MobileNet(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                **kwargs)
    return model

def quantmobilenet_4_mult(num_classes=1000, **kwargs):
    bit = 4
    model = MobileNet(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                **kwargs)
    return model

def quantmobilenet_2_mult(num_classes=1000, **kwargs):
    bit = 2
    model = MobileNet(qm.QuantActivConv2d, qm.HWGQ, bit=bit,
                 num_classes=num_classes,
                **kwargs)
    return model


if __name__ == '__main__':
    x = torch.randn(1, 3, 224, 224)

    net = quantmobilenet_32_mult(100)
    out = net(x)

    # print(out.shape)
    # print(net)
    feats, out = net.extract_feature(x)
  #  feats, logit = net(x, is_feat=True, preact=False)
    for f in feats:
        print(f.shape, f.min().item())
    # print(logit.shape)
   # print(net.blocks)