import torch
import torch.nn as nn
import math
from torch.nn.parameter import Parameter
import torch.nn.functional as F
from modules_q import quant_mult as qm

__all__ = [
    "quantres18_32_864_mult", 'quantres18_32_42_mult', 'quantres18_32_4_mult', 'quantres18_432_mult',
    "quantres18_42_mult", "quantres18_43_mult",
    "quantres18_864_mult", "quantres18_84_mult", "quantres18_842_mult", "quantres18_32_84_mult"
]


class BasicBlock(nn.Module):
    expansion = 1
    num_layers = 2

    def __init__(self, conv_func, act_func, inplanes, planes, bit, stride=1,
                 downsample=None, bnaff=True, **kwargs):
        super(BasicBlock, self).__init__()
       # self.bn3 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv1 = conv_func(inplanes, planes, bit, kernel_size=3, stride=stride,
                               padding=1, bias=False, **kwargs)
        self.act1 = act_func(bit)
        self.bn1 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv2 = conv_func(planes, planes, bit, kernel_size=3, stride=1,
                               padding=1, bias=False, **kwargs)
        self.bn2 = nn.BatchNorm2d(planes)
        self.act2 = act_func(bit)
        self.downsample = downsample

    def forward(self, x):
        residual = x
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.act1(out)
        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)
        out += residual
     #   out = self.bn3(out)
        out = self.act2(out)

        return out


class Bottleneck(nn.Module):
    expansion = 4
    num_layers = 3

    def __init__(self, conv_func, inplanes, planes, archws, archas, stride=1,
                 downsample=None, bnaff=True, **kwargs):
        super(Bottleneck, self).__init__()
        assert len(archws) == 3
        assert len(archas) == 3
        self.bn0 = nn.BatchNorm2d(inplanes, affine=bnaff)
        self.conv1 = conv_func(inplanes, planes, archws[0], archas[0], kernel_size=1, bias=False, **kwargs)
        self.bn1 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv2 = conv_func(planes, planes, archws[1], archas[1], kernel_size=3, stride=stride,
                               padding=1, bias=False, **kwargs)
        self.bn2 = nn.BatchNorm2d(planes, affine=bnaff)
        self.conv3 = conv_func(
            planes, planes * 4, archws[2], archas[2], kernel_size=1, bias=False, **kwargs)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.downsample = downsample

    def forward(self, x):
        out = self.bn0(x)
        if self.downsample is not None:
            residual = out
        else:
            residual = x

        out = self.conv1(out)
        out = self.bn1(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(residual)

        out += residual

        return out


class ResNet(nn.Module):

    def __init__(self, block, conv_func, act_func, layers, bits, num_classes=1000,
                 bnaff=True, **kwargs):
        self.inplanes = 64
        self.conv_func = conv_func
        self.act_func = act_func
        super(ResNet, self).__init__()

        self.bits = bits
        self.alpha_activ_layer1 = Parameter(torch.Tensor(len(self.bits)))
        self.alpha_activ_layer1.data.fill_(0.01)
        self.alpha_activ_layer2 = Parameter(torch.Tensor(len(self.bits)))
        self.alpha_activ_layer2.data.fill_(0.01)
        self.alpha_activ_layer3 = Parameter(torch.Tensor(len(self.bits)))
        self.alpha_activ_layer3.data.fill_(0.01)
        self.alpha_activ_layer4 = Parameter(torch.Tensor(len(self.bits)))
        self.alpha_activ_layer4.data.fill_(0.01)

        #  self.conv1_list = nn.ModuleList()
        #   self.bn1_list = nn.ModuleList()
        # self.relu_list = nn.ModuleList()
        #    self.maxpool_list = nn.ModuleList()
        self.layer1_list = nn.ModuleList()
        self.layer2_list = nn.ModuleList()
        self.layer3_list = nn.ModuleList()
        self.layer4_list = nn.ModuleList()
        self.avgpool_list = nn.ModuleList()

        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.bn1 = nn.BatchNorm2d(64)
        for bit in self.bits:
            #  self.conv1_list.append(nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False))

            #    self.bn1_list.append(nn.BatchNorm2d(64))
            #   self.relu_list.append(nn.ReLU(inplace=True))
            #     self.maxpool_list.append(nn.MaxPool2d(kernel_size=3, stride=2, padding=1))
            self.layer1_list.append(self._make_layer(
                block, conv_func, act_func, 64, 64, layers[0], bit, bnaff=bnaff, **kwargs))
            self.layer2_list.append(self._make_layer(
                block, conv_func, act_func, 64, 128, layers[1], bit, stride=2, bnaff=bnaff, **kwargs))
            self.layer3_list.append(self._make_layer(
                block, conv_func, act_func, 128, 256, layers[2], bit, stride=2, bnaff=bnaff, **kwargs))
            self.layer4_list.append(self._make_layer(
                block, conv_func, act_func, 256, 512, layers[3], bit, stride=2, bnaff=bnaff, **kwargs))
        self.relu = act_func(bit)
        self.avgpool = nn.AvgPool2d(7)

        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                if m.weight is not None:
                    m.weight.data.fill_(1)
                if m.bias is not None:
                    m.bias.data.zero_()
        self.reset_parameters(zero_init_residual=True)
      #  if num_classes == 1000:
       #     self.load_pretrain_model()

    def _make_layer(self, block, conv_func, act_func, inplane, planes, blocks, bit, stride=1, bnaff=True, **kwargs):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv_func(inplane, planes * block.expansion, bit=bit, kernel_size=1,
                          stride=stride, bias=False, **kwargs),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(
            block(conv_func, act_func, inplane, planes, bit=bit, stride=stride, downsample=downsample, bnaff=bnaff,
                  **kwargs))
        inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(conv_func, act_func, inplanes, planes, bit=bit, bnaff=bnaff, **kwargs))

        return nn.Sequential(*layers)

    def forward(self, x, extract=False):
        out_layer1 = []
        out_layer2 = []
        out_layer3 = []
        out_layer4 = []
        sw1 = F.softmax(self.alpha_activ_layer1, dim=0)
        sw2 = F.softmax(self.alpha_activ_layer2, dim=0)
        sw3 = F.softmax(self.alpha_activ_layer3, dim=0)
        sw4 = F.softmax(self.alpha_activ_layer4, dim=0)

        out_x = self.conv1(x)
        out_x = self.maxpool(out_x)
        out_x = self.bn1(out_x)
        out_x = self.relu(out_x)
        for i, bit in enumerate(self.bits):
            # out_x = self.conv1_list[i](x)
            # out_x = self.maxpool_list[i](out_x)
            # out_x = self.bn1_list[i](out_x)
            out_layer1.append(self.layer1_list[i](out_x) * sw1[i])
        activ_layer1 = sum(out_layer1)
        for i, bit in enumerate(self.bits):
            out_layer2.append(self.layer2_list[i](activ_layer1) * sw2[i])
        activ_layer2 = sum(out_layer2)
        for i, bit in enumerate(self.bits):
            out_layer3.append(self.layer3_list[i](activ_layer2) * sw3[i])
        activ_layer3 = sum(out_layer3)
        for i, bit in enumerate(self.bits):
            out_layer4.append(self.layer4_list[i](activ_layer3) * sw4[i])
        activ_layer4 = sum(out_layer4)
        assert activ_layer4.shape[2] == 7
        assert activ_layer4.shape[3] == 7
        out = self.avgpool(activ_layer4)
        pool = out
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        if extract:
            return [activ_layer1, activ_layer2, activ_layer3, activ_layer4, pool], out
        else:
            return out

    def get_channel_num(self):
        return [64, 128, 256, 512, 512]

    def reset_parameters(self, zero_init_residual):

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

    def load_pretrain_model(self):
        origin = resnet18(pretrained=True, progress=True)
   #     checkpoints = torch.load("/home/cuongpv/emips_kd/checkpoints/quantres18_864_mult/fitnet_KD_imagenetfull/teacher/model_best_ImageNet.pth.tar", map_location=lambda storage, loc: storage)
    #    state_dict = checkpoints["state_dict"]
     #   origin.load_state_dict(state_dict, strict=False)
        for layer in self.layer1_list.modules():
            layer.load_state_dict(origin.layer1.state_dict(), strict=False)
        for layer in self.layer2_list.modules():
            layer.load_state_dict(origin.layer2.state_dict(), strict=False)
        for layer in self.layer3_list.modules():
            layer.load_state_dict(origin.layer3.state_dict(), strict=False)
        for layer in self.layer4_list.modules():
            layer.load_state_dict(origin.layer4.state_dict(), strict=False)
        self.fc.load_state_dict(origin.fc.state_dict(), strict=False)
        self.conv1.load_state_dict(origin.conv1.state_dict(), strict=False)
        self.bn1.load_state_dict(origin.bn1.state_dict(), strict=False)

    def show_params(self):
        for name, param in self.named_parameters():
            if "alpha_activ" in name:
                print(name, param)


def quantres18_42_mult(**kwargs):
    bits = [4, 2]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_432_mult(**kwargs):
    bits = [4, 3, 2]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_43_mult(**kwargs):
    bits = [4, 3]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_32_4_mult(**kwargs):
    bits = [32, 4]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_32_42_mult(**kwargs):
    bits = [32, 4, 2]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_32_84_mult(**kwargs):
    bits = [32, 8, 4]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_32_842_mult(**kwargs):
    bits = [32, 8, 4, 2]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_32_864_mult(**kwargs):
    bits = [32, 8, 6, 4]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)

def quantres18_84_mult(**kwargs):
    bits = [8, 4]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_864_mult(**kwargs):
    bits = [8, 6, 4]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


def quantres18_842_mult(**kwargs):
    bits = [8, 4, 2]
    return ResNet(BasicBlock, qm.QuantConv2d, qm.HWGQ, [2, 2, 2, 2], bits, **kwargs)


from torch.utils.model_zoo import load_url

model_urls = {
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}
from torchvision.models import resnet18

if __name__ == "__main__":
    model = quantres18_864_mult()
    x = torch.randn(1, 3, 224, 224)
    out = model(x)
    # origin = resnet18(pretrained=True, progress=True)
    # for layer in model.layer1_list.modules():
    #     layer.load_state_dict(origin.layer1.state_dict(), strict=False)
    # for layer in model.layer2_list.modules():
    #     layer.load_state_dict(origin.layer2.state_dict(), strict=False)
    # for layer in model.layer3_list.modules():
    #     layer.load_state_dict(origin.layer3.state_dict(), strict=False)
    # for layer in model.layer4_list.modules():
    #     layer.load_state_dict(origin.layer4.state_dict(), strict=False)
    # model.fc.load_state_dict(origin.fc.state_dict(), strict=False)
    # model.conv1.load_state_dict(origin.conv1.state_dict(), strict=False)
    # model.bn1.load_state_dict(origin.bn1.state_dict(), strict=False)
    print(model)
    print(model.show_params())

#   for name, params in origin.layer1.state_dict():
#      print(name)
#  print(origin.layer1.state_dict().keys())
# layer.load_state_dict(origin.layer1.state_dict(), strict=False)

# feats = model(x, extract=True)
# for feat in feats:
#     print(feat.shape)
#  print(out.shape)
# print(model)
