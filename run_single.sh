python main_single.py -a quantres18_32_mult \
              --epochs 150 --step-epoch 50 -j 4 -b 256 \
              --type_data CIFAR100 \
              --lr 0.1 --wd 1e-4 --gpu 0 --use_wandb

python main_single.py -a quantres18_8_mult \
              --epochs 150 --step-epoch 50 -j 4 -b 256 \
              --type_data CIFAR100 \
              --lr 0.1 --wd 1e-4 --gpu 0 --use_wandb


python main_single.py -a quantres18_6_mult \
              --epochs 150 --step-epoch 50 -j 4 -b 256 \
              --type_data CIFAR100 \
              --lr 0.1 --wd 1e-4 --gpu 0 --use_wandb

python main_single.py -a quantres18_4_mult \
              --epochs 150 --step-epoch 50 -j 4 -b 256 \
              --type_data CIFAR100 \
              --lr 0.1 --wd 1e-4 --gpu 0 --use_wandb

python main_single.py -a quantres18_2_mult \
              --epochs 150 --step-epoch 50 -j 4 -b 256 \
              --type_data CIFAR100 \
              --lr 0.1 --wd 1e-4 --gpu 0 --use_wandb

#python main_single.py -a quantres18_2_mult \
#              --epochs 50 --step-epoch 10 -j 4 -b 256 \
#              --type_data ImageNet \
#              --lr 0.1 --wd 1e-4 --gpu 1 \
#              --data ../data/imagenet --use_wandb
