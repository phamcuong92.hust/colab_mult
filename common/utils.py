import torch
import os
import shutil


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].reshape(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def adjust_learning_rate(optimizer, epoch, args):
    """Sets the learning rate to the initial LR decayed by 10 every step_epochs"""
    lr = args.lr * (0.1 ** (epoch // args.step_epoch))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

import json
def adjust_learning_rate_mobi(optimizer, epoch, args):
    epoch_step = json.loads(args.epoch_step)
    if epoch in epoch_step:
        lr = optimizer.param_groups[0]['lr']
        optimizer.param_groups[0]['lr'] = lr * args.lr_decay_ratio
def adjust_learning_rate_t(optimizer, epoch, args):
    """Sets the learning rate to the initial LR decayed by 10 every step_epochs"""
    lr_t = args.lr_t * (0.1 ** (epoch // args.step_epoch))
    optimizer.param_groups[0]["lr"] = lr_t


# def save_checkpoint(path_save, state, is_best, epoch, step_epoch, filename='arch_checkpoint.pth.tar'):
#     if not os.path.exists(path_save):
#         os.makedirs(path_save)
#     torch.save(state, filename)
#     shutil.copyfile(filename, path_save + "/" + filename)
#     if is_best:
#         shutil.copyfile(filename, path_save + "/" + 'arch_model_best.pth.tar')
#     if (epoch + 1) % step_epoch == 0:
#         name_checkpoint = 'arch_checkpoint_ep{}.pth.tar'.format(epoch + 1)
#         shutil.copyfile(filename, path_save + "/" + name_checkpoint)


# def save_checkpoint(path_save, state, is_best, epoch, step_epoch, type_data):
#     if not os.path.exists(path_save):
#         os.makedirs(path_save)
#     #  path = os.path.join(path_save, type_data)
#     filename = path_save + "/" + "arch_checkpoint_direct" + "_" + type_data + ".pth.tar"
#     torch.save(state, filename)
#   #  shutil.copyfile(filename, path_save + "/" + filename)
#     if is_best:
#         best_checkpoint = "arch_model_direct_best" + "_" + type_data + ".pth.tar"
#         shutil.copyfile(filename, path_save + "/" + best_checkpoint)
#     # if (epoch + 1) % step_epoch == 0:
#     #     name_checkpoint = 'arch_checkpoint_direct_' + type_data + '_ep{}.pth.tar'.format(epoch + 1)
#     #     shutil.copyfile(filename, path_save + "/" + name_checkpoint)


def save_checkpoint(path_save, state, is_best, type_data):
    if not os.path.exists(path_save):
        os.makedirs(path_save)
    filename = path_save + "/" + "checkpoint" + "_" + type_data + ".pth.tar"
    torch.save(state, filename)
    if is_best:
        best_checkpoint = "model_best" + "_" + type_data + ".pth.tar"
        shutil.copyfile(filename, path_save + "/" + best_checkpoint)
