import argparse
import os
import random
import shutil
import time
import warnings
from PIL import Image
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
import socket
from datasets import dataset_loader as ds

import models as models
from common.utils import *
from metric.loss import distillation
from training.mult_origin_kd import train, validate

import wandb

model_names = sorted(name for name in models.__dict__
    if name.islower() and not name.startswith("__")
    and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument(
    "--use_wandb", default=False, action="store_true", help="If true, do not use wandb for this run",
)

parser.add_argument(
    "--test_resnet_small", default=False, action="store_true", help="If true, use resnet small for this run",
)

parser.add_argument('--data', type=str, default="/datassd/common/imagenet/", help='Dataset folder') # path to dataset
parser.add_argument('--arch-cfg', '--ac', default='', type=str, metavar='PATH',
                    help='path to architecture configuration')
parser.add_argument('-a', '--arch', metavar='ARCH', default='resnet18',
                    help='model architecture: ' +
                         ' | '.join(model_names) +
                         ' (default: resnet18)')
parser.add_argument('-t', '--arch_t', metavar='ARCH', default='resnet18',
                    help='model architecture: ' +
                         ' | '.join(model_names) +
                         ' (default: mixres18_w1234a234_emips_kd)')
parser.add_argument('-j', '--workers', default=16, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--image_size', default=224, type=int, metavar='N',
                    help='Size of image training')
parser.add_argument('--epochs', default=90, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--step-epoch', default=30, type=int, metavar='N',
                    help='number of epochs to decay learning rate')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('-b', '--batch-size', default=256, type=int,
                    metavar='N',
                    help='mini-batch size (default: 256), this is the total '
                         'batch size of all GPUs on the current node when '
                         'using Data Parallel or Distributed Data Parallel')
parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    metavar='LR', help='initial learning rate', dest='lr')
parser.add_argument('--lra', '--learning-rate-alpha', default=0.01, type=float,
                    metavar='LR', help='initial alpha learning rate')
parser.add_argument('--lrc', '--learning-rate-centroid', default=0.005, type=float,
                    metavar='LR', help='initial alpha learning rate')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--wd', '--weight-decay', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)',
                    dest='weight_decay')

parser.add_argument('--qwd', '--quant_weight_decay', default=1e-7, type=float,
                    metavar='W', help='quant weight decay (default: 1e-7)',
                    dest='quant_weight_decay')

parser.add_argument('--complexity-decay', '--cd', default=0, type=float,
                    metavar='W', help='complexity decay (default: 1e-4)')
parser.add_argument('--kd_parameter', '--kd', default=1.0, type=float,
                    metavar='W', help='complexity decay (default: 1e-4)')
parser.add_argument('-p', '--print-freq', default=100, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                     help='path to latest checkpoint (default: none)')

# parser.add_argument(
#     "--resume_", default=False, action="store_true", help="If true, automatic resume weight in folder for this run",
# )
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')
parser.add_argument('--world-size', default=-1, type=int,
                    help='number of nodes for distributed training')
parser.add_argument('--rank', default=-1, type=int,
                    help='node rank for distributed training')
parser.add_argument('--dist-url', default='tcp://224.66.41.62:23456', type=str,
                    help='url used to set up distributed training')
parser.add_argument('--dist-backend', default='nccl', type=str,
                    help='distributed backend')
parser.add_argument('--seed', default=None, type=int,
                    help='seed for initializing training. ')
parser.add_argument('--gpu', default=None, type=int,
                    help='GPU id to use.')

parser.add_argument('--optim', default="SGD", type=str,
                    help='optimziation method.')
parser.add_argument("--path_save", default="checkpoint", type=str,
                    help="path of the checkpoint model")

parser.add_argument('--init_slope', default=1., type=float,
                    help='inital STE slope')
parser.add_argument('--end_slope', default=100., type=float,
                    help='final STE slope')
parser.add_argument('--end_lr', default=1e-5, type=float,
                    help='final learning rate')
parser.add_argument('--multiprocessing-distributed', action='store_true',
                    help='Use multi-processing distributed training to launch '
                         'N processes per node, which has N GPUs. This is the '
                         'fastest way to use PyTorch for either single node or '
                         'multi node data parallel training')

parser.add_argument('--type_data', default='ImageNet', type=str,
                    choices=['CIFAR10', 'CIFAR100', 'ImageNet'],
                    help='type of data to training')
parser.add_argument('--T', type=float, default=4.0, metavar='Temputure', help='Temputure for distillation')
parser.add_argument('--alpha_kd', type=float, default=0.5)  # weight for ce and kl
best_acc1 = 0
best_acc5 = 0
comp_name = socket.gethostname()
def main():
    args = parser.parse_args()
    print(args)


    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. '
                      'This will turn on the CUDNN deterministic setting, '
                      'which can slow down your training considerably! '
                      'You may see unexpected behavior when restarting '
                      'from checkpoints.')

    if args.gpu is not None:
        warnings.warn('You have chosen a specific GPU. This will completely '
                      'disable data parallelism.')

    if args.dist_url == "env://" and args.world_size == -1:
        args.world_size = int(os.environ["WORLD_SIZE"])

    args.distributed = args.world_size > 1 or args.multiprocessing_distributed

    ngpus_per_node = torch.cuda.device_count()
    if args.multiprocessing_distributed:
        # Since we have ngpus_per_node processes per node, the total world_size
        # needs to be adjusted accordingly
        args.world_size = ngpus_per_node * args.world_size
        # Use torch.multiprocessing.spawn to launch distributed processes: the
        # main_worker process function
        mp.spawn(main_worker, nprocs=ngpus_per_node, args=(ngpus_per_node, args))
    else:
        # Simply call main_worker function
        main_worker(args.gpu, ngpus_per_node, args)


def main_worker(gpu, ngpus_per_node, args):
    global best_acc1
    global best_acc5
    if args.use_wandb:
        wandb.init(project="Main distillation Multi teacher")
    args.gpu = gpu

    if args.gpu is not None:
        print("Use GPU: {} for training".format(args.gpu))

    if args.distributed:
        if args.dist_url == "env://" and args.rank == -1:
            args.rank = int(os.environ["RANK"])
        if args.multiprocessing_distributed:
            # For multiprocessing distributed training, rank needs to be the
            # global rank among all the processes
            args.rank = args.rank * ngpus_per_node + gpu
        dist.init_process_group(backend=args.dist_backend, init_method=args.dist_url,
                                world_size=args.world_size, rank=args.rank)
    # create model
    print("=> creating model '{}'".format(args.arch))

    if args.type_data == "ImageNet":
        model = models.__dict__[args.arch](num_classes=1000)
        model_t = models.__dict__[args.arch_t](num_classes=1000)
    elif args.type_data == "CIFAR100":
        model = models.__dict__[args.arch](num_classes=100)
        model_t = models.__dict__[args.arch_t](num_classes=100)
    elif args.type_data == "CIFAR10":
        model = models.__dict__[args.arch](num_classes=10)
        model_t = models.__dict__[args.arch_t](num_classes=10)

    checkpoint = torch.load(args.arch_cfg)
    model_t.load_state_dict(checkpoint['state_dict']) # load pretrain model
    model_t.eval()
    if args.use_wandb:
        wandb.watch(model)
    if args.distributed:
        # For multiprocessing distributed, DistributedDataParallel constructor
        # should always set the single device scope, otherwise,
        # DistributedDataParallel will use all available devices.
        if args.gpu is not None:
            torch.cuda.set_device(args.gpu)
            model.cuda(args.gpu)
            # When using a single GPU per process and per
            # DistributedDataParallel, we need to divide the batch size
            # ourselves based on the total number of GPUs we have
            args.batch_size = int(args.batch_size / ngpus_per_node)
            args.workers = int((args.workers + ngpus_per_node - 1) / ngpus_per_node)
            model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[args.gpu])
        else:
            model.cuda()
            # DistributedDataParallel will divide and allocate batch_size to all
            # available GPUs if device_ids are not set
            model = torch.nn.parallel.DistributedDataParallel(model)
    elif args.gpu is not None:
        torch.cuda.set_device(args.gpu)
        model = model.cuda(args.gpu)
        model_t = model_t.cuda(args.gpu)
    else:
        # DataParallel will divide and allocate batch_size to all available GPUs
        if 'alex' in args.arch or 'vgg' in args.arch:
            model.features = torch.nn.DataParallel(model.features)
            model.cuda()
        else:
            model = torch.nn.DataParallel(model).cuda()

    # define loss function (criterion) and optimizer
    criterion = nn.CrossEntropyLoss().cuda(args.gpu)
    optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                momentum=args.momentum,
                                weight_decay=args.weight_decay)
    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            if args.gpu is None:
                checkpoint = torch.load(args.resume)
            else:
                # Map model to be loaded to specified single gpu.
                loc = 'cuda:{}'.format(args.gpu)
                checkpoint = torch.load(args.resume, map_location=loc)
            args.start_epoch = checkpoint['epoch']
            best_acc1 = checkpoint['best_acc1']
            if args.gpu is not None:
                # best_acc1 may be from a checkpoint from a different GPU
                best_acc1 = best_acc1.to(args.gpu)
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    cudnn.benchmark = True
    # Get dataloader
    if 'inception' in args.arch:
        #  crop_size, short_size = 299, 342
        crop_size, short_size = 299, 342
    elif args.image_size == 32:
        crop_size, short_size = 32, 32
    else:
        crop_size, short_size = 224, 256
    if 'alex' in args.arch:
        crop_size, short_size = 227, 256
    train_loader, val_loader, train_sampler = ds.get_dataset(ds.Dataset[args.type_data], args.data,
                                                             batch_size=args.batch_size,
                                                             num_workers=args.workers,
                                                             crop_size=crop_size, short_size=short_size,
                                                             distributed=False,
                                                             enable_auto_augmentation=False)


    if args.evaluate:
        validate(val_loader, model, criterion, args)
        return
    # check acc teacher model
    acc1_t, acc5_t, loss_va_tl = validate(val_loader, model_t, criterion, args)
    print("accuracy teacher model {}. {}".format(acc1_t, acc5_t))
    best_epoch = args.start_epoch
    for epoch in range(args.start_epoch, args.epochs):
        if args.distributed:
            train_sampler.set_epoch(epoch)
        adjust_learning_rate(optimizer, epoch, args)

        # train for one epoch
        print("train epoch")
        loss_train = train(train_loader, model, model_t, distillation, optimizer, epoch, args)

        # evaluate on validation set
        acc1, acc5, loss_val = validate(val_loader, model, criterion, args)

        # remember best acc@1 and save checkpoint
        is_best = acc1 > best_acc1
        best_acc1 = max(acc1, best_acc1)
        if is_best:
            best_epoch = epoch
            best_acc5 = acc5
        # get lr and slope
        for param_group in optimizer.param_groups:
            lr = param_group['lr']
            break

        print('[' + comp_name + time.strftime(" %m/%d %H:%M:%S] ")
            + ' * Best Acc@1 {:.3f} - Best Acc@5 {:.3f} - Learning rate {:.7f}\n'.format(best_acc1, best_acc5, lr))

        if not args.multiprocessing_distributed or (args.multiprocessing_distributed
                and args.rank % ngpus_per_node == 0):
            path_checkpoint = "/".join(args.arch_cfg.split("/")[:-1]) + "/origin_KD"
            save_checkpoint(path_checkpoint, {
                'epoch': epoch + 1,
                'arch': args.arch,
                'state_dict': model.state_dict(),
                'best_acc1': best_acc1,
                'optimizer': optimizer.state_dict()
            }, is_best, args.type_data)
        metrics = {'train_loss': loss_train,
                   "val_loss": loss_val,
                   "val_acc1": acc1,
                   "val_acc5": acc5
                   }
        if args.use_wandb:
            wandb.log(metrics)
    print('Best Acc@1 {0} Best Acc@5 {1} @ epoch {2}'.format(best_acc1, best_acc5, best_epoch))


if __name__ == '__main__':
    main()

