python main_mult_fitnet_from_scratch.py -t quantres18_864_mult -a quantres18_2_mult \
                      --epochs 150 --step-epoch 50 -j 4 -b 64 \
                      --type_data CIFAR100 \
                      --ac ./checkpoints/quantres18_864_mult/model_best_CIFAR100.pth.tar \
                      --lr 0.1 --lra 0.001 \
                      --lr_t 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb


python main_mult_fitnet.py -t quantres18_864_mult -a quantres18_2_mult \
                      --epochs 150 --step-epoch 50 -j 4 -b 64 \
                      --type_data CIFAR100 \
                      --ac ./checkpoints/quantres18_864_mult/model_best_CIFAR100.pth.tar \
                      --lr 0.1 --lra 0.001 \
                      --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb


python main_mult_fitnet_from_scratch.py -t quantres18_842_mult -a quantres18_2_mult \
                      --epochs 150 --step-epoch 50 -j 4 -b 64 \
                      --type_data CIFAR100 \
                      --ac ./checkpoints/quantres18_842_mult/model_best_CIFAR100.pth.tar \
                      --lr 0.1 --lra 0.001 \
                      --data ../data/ \
                      --lr_t 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 1 --use_wandb


python main_mult_fitnet_from_scratch.py -t quantres18_32_84_mult -a quantres18_2_mult \
                      --epochs 150 --step-epoch 50 -j 4 -b 64 \
                      --type_data CIFAR100 \
                      --ac ./checkpoints/quantres18_32_84_mult/model_best_CIFAR100.pth.tar \
                      --lr 0.1 --lra 0.001 \
                      --lr_t 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb

python main_mult_trained_fitnet.py -t quantres18_864_mult \
          -a quantres18_2_mult \
          --epochs 150 --step-epoch 50 -j 4 -b 256 \
          --type_data CIFAR100 --ac ./checkpoints/quantres18_864_mult/model_best_CIFAR100.pth.tar \
          --lr 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb


python main_baseline1.py -t quantres18_8_mult quantres18_6_mult quantres18_4_mult \
-a quantres18_2_mult \
--epochs 150 --step-epoch 50 -j 4 -b 256 \
--type_data CIFAR100 \
--ac {./checkpoints/quantres18_8_mult/model_best_CIFAR100.pth.tar, ./checkpoints/quantres18_6_mult/model_best_CIFAR100.pth.tar, ./checkpoints/quantres18_4_mult/model_best_CIFAR100.pth.tar} \
--lr 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb



python main_mult_fitnet_from_scratch.py -t quantres18_864_mult -a quantres18_2_mult \
                      --epochs 100 --step-epoch 30 -j 4 -b 64 \
                      --type_data ImageNet \
                      --ac ./checkpoints/quantres18_864_mult/model_best_ImageNet.pth.tar \
                      --lr 0.1 --lra 0.001 \
                      --data ../data/imagenet \
                      --lr_t 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 1 --use_wandb


python main_mult_fitnet_from_scratch.py -t quantres18_842_mult -a quantres18_1_mult \
                      --epochs 150 --step-epoch 50 -j 4 -b 64 \
                      --type_data CIFAR100 \
                      --ac ./checkpoints/quantres18_842_mult/model_best_CIFAR100.pth.tar \
                      --lr 0.1 --lra 0.001 \
                      --lr_t 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb



python main_single.py -a quantres18_1_mult --epochs 150 --step-epoch 50 -j 4 -b 256 --type_data CIFAR100 --lr 0.1 --wd 1e-4 --gpu 0 --use_wandb


python main_baseline1.py \
-t quantres18_8_mult quantres18_4_mult quantres18_2_mult -a quantres18_1_mult --epochs 150 --step-epoch 50 -j 4 -b 256 --type_data CIFAR100 --ac {./checkpoints/quantres18_8_mult/model_best_CIFAR100.pth.tar, ./checkpoints/quantres18_6_mult/model_best_CIFAR100.pth.tar, ./checkpoints/quantres18_4_mult/model_best_CIFAR100.pth.tar} --lr 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb




python main_mult_trained_fitnet.py \
      -t quantres18_32_42_mult -a quantres18_2_mult \
      --epochs 50 --step-epoch 10 -j 4 -b 256 \
      --type_data ImageNet \
      --ac ./checkpoints_pa/quantres18_32_42_mult/model_best_ImageNet.pth.tar --lr 0.1 --lra 0.001 --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 1 --use_wandb

python main_mult_fitnet_from_scratch.py \
      -t quantres18_864_mult -a quantres18_2_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 64 \
      --type_data ImageNet \
      --ac ./checkpoints/quantres18_864_mult/model_best_ImageNet.pth.tar --lr 0.1 --lra 0.001 \
      --lr_t 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 1 -p 2000 --use_wandb