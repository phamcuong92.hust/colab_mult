from common.utils import AverageMeter, ProgressMeter, accuracy
import torch
import time
import torch.nn.functional as F
import torch.nn as nn
# avg logits
# input: Tensor[3, 128, 10]
def avg_logits(te_scores_Tensor):
#     print(te_scores_Tensor.size())
    mean_Tensor = torch.mean(te_scores_Tensor, dim=1)
#     print(mean_Tensor)
    return mean_Tensor

def train(train_loader, model, model_t1, model_t2, model_t3,fitnet_criterion, optimizer, optimizer1, optimizer2, optimizer3, epoch, args):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix="Epoch: [{}/{}]\t".format(epoch, args.epochs))

    # switch to train mode
    model.train()

    model_t1.train()
    model_t2.train()
    model_t3.train()

    end = time.time()
    for i, (images, target) in enumerate(train_loader):
        #  print("start training")
        # measure data loading time
        fit_loss = 0
        data_time.update(time.time() - end)

        if args.gpu is not None:
            images = images.cuda(args.gpu, non_blocking=True)
        target = target.cuda(args.gpu, non_blocking=True)
        batch_size = images.shape[0]

        # compute output
        b1, b2, b3, b4, pool, output = model(images, extract=True)  # out_feat: 16, 32, 64, 64, -
        st_maps = [b1, b2, b3, b4]


        t1_b1, t1_b2, t1_b3, t1_b4, t1_pool, t1_output = model_t1(images, extract=True)
        t2_b1, t2_b2, t2_b3, t2_b4, t2_pool, t2_output = model_t2(images, extract=True)
        t3_b1, t3_b2, t3_b3, t3_b4, t3_pool, t3_output = model_t3(images, extract=True)
      #  print(output.shape)
        logits = F.softmax(output, dim=1)
       # print(logits.shape)
        logits_1 = F.softmax(t1_output, dim=1)
        logits_2 = F.softmax(t2_output, dim=1)
        logits_3 = F.softmax(t3_output, dim=1)

        t_b1_tensor = torch.stack([t1_b1, t2_b1, t3_b1],dim=1)
        t_b2_tensor = torch.stack([t1_b2, t2_b2, t3_b2], dim=1)
        t_b3_tensor = torch.stack([t1_b3, t2_b3, t3_b3], dim=1)
        t_b4_tensor = torch.stack([t1_b4, t2_b4, t3_b4], dim=1)
        t_pool_tensor = torch.stack([t1_pool, t2_pool], dim=1)

        mean_t_b1 = avg_logits(t_b1_tensor)
        mean_t_b2 = avg_logits(t_b2_tensor)
        mean_t_b3 = avg_logits(t_b3_tensor)
        mean_t_b4 = avg_logits(t_b4_tensor)
        mean_t_pool = avg_logits(t_pool_tensor)
        hint_maps_avg = [mean_t_b1, mean_t_b2, mean_t_b3, mean_t_b4]
        feat_num = len(hint_maps_avg)
        for j, fitnet in enumerate(fitnet_criterion):
            fit_loss += fitnet(st_maps[j], hint_maps_avg[j])/2**(feat_num - j - 1)
        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss = 0.5 * (nn.KLDivLoss()(logits_3, logits) + nn.KLDivLoss()(logits_2, logits) + nn.KLDivLoss()(logits_1, logits)) + F.cross_entropy(
            output, target)
        loss = loss + fit_loss/batch_size
        loss.backward(retain_graph=True)
        optimizer.step()

        optimizer1.zero_grad()
        loss1 = 0.5 * (nn.KLDivLoss()(logits_2, logits_1) + nn.KLDivLoss()(logits_3, logits_1) + nn.KLDivLoss()(logits.detach(), logits_1)) + F.cross_entropy(
            t1_output, target)
        loss1.backward(retain_graph=True)
        optimizer1.step()

        optimizer2.zero_grad()
        loss2 = 0.5 * (nn.KLDivLoss()(logits_1.detach(), logits_2) + nn.KLDivLoss()(logits_3, logits_2) + nn.KLDivLoss()(logits.detach(), logits_2)) + F.cross_entropy(
            t2_output, target)
        loss2.backward(retain_graph=True)
        optimizer2.step()

        optimizer3.zero_grad()
        loss3 = 0.5 * (nn.KLDivLoss()(logits_1.detach(), logits_3) + nn.KLDivLoss()(logits_2.detach(), logits_3) + nn.KLDivLoss()(logits.detach(), logits_3)) + F.cross_entropy(
            t3_output, target)
        loss3.backward()
        optimizer3.step()
        # measure elapsed time

        losses.update(loss.item(), images.size(0))
        batch_time.update(time.time() - end)
        end = time.time()
        if i % args.print_freq == 0:
            progress.display(i)
    return losses.avg


def validate(val_loader, model, criterion, args):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time, losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            output = model(images)
            loss = criterion(output, target)

            # measure accuracy and record loss
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                progress.display(i)

        # TODO: this should also be done with the ProgressMeter
        print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))

    return top1.avg, top5.avg, losses.avg
