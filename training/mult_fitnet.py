from common.utils import AverageMeter, ProgressMeter, accuracy
import torch
import time


def train(train_loader, model, teacher_model, loss_fn, fitnet_criterion, optimizer, optimizer_t, epoch, args):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix="Epoch: [{}/{}]\t".format(epoch, args.epochs))

    # switch to train mode
    model.train()
    teacher_model.train()

    end = time.time()
    for i, (images, target) in enumerate(train_loader):
        #  print("start training")
        # measure data loading time
        data_time.update(time.time() - end)

        if args.gpu is not None:
            images = images.cuda(args.gpu, non_blocking=True)
        target = target.cuda(args.gpu, non_blocking=True)
        batch_size = images.shape[0]

        # compute output
        b1, b2, b3, b4, pool, output = model(images, extract=True)  # out_feat: 16, 32, 64, 64, -
      #  st_maps = [b1, b2, b3, b4, pool]
        st_maps = [b1, b2, b3, b4]
        #  with torch.no_grad():
        t_b1, t_b2, t_b3, t_b4, t_pool, t_output = teacher_model(images, extract=True)
      #  hint_maps = [t_b1, t_b2, t_b3, t_b4, t_pool]
        hint_maps = [t_b1, t_b2, t_b3, t_b4]
        feat_num = len(hint_maps)
        fit_loss = 0
        for j, fitnet in enumerate(fitnet_criterion):
            fit_loss += fitnet(st_maps[j], hint_maps[j])/2**(feat_num - j - 1)
        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

        # compute gradient and do SGD step
        if epoch < 11:
            optimizer.zero_grad()
            loss = loss_fn(output, target, t_output, T=args.T, alpha=0)
            loss = loss + fit_loss/(1000*batch_size)
            loss.backward(retain_graph=True)
            optimizer.step()

            optimizer_t.zero_grad()
            loss_t = loss_fn(t_output, target, output.detach(), T=args.T, alpha=0)
            loss_t = loss_t + fit_loss.detach()/(1000*batch_size)
            loss_t.backward(retain_graph=True)
            optimizer_t.step()
        else:
            optimizer.zero_grad()
            loss = loss_fn(output, target, t_output, T=args.T, alpha=args.alpha_kd)
            loss = loss + fit_loss/(1000*batch_size)
            loss.backward(retain_graph=True)
            optimizer.step()

            optimizer_t.zero_grad()
            loss_t = loss_fn(t_output, target, output.detach(), T=args.T, alpha=args.alpha_kd)
            loss_t = loss_t + fit_loss.detach()/(1000*batch_size)
            loss_t.backward(retain_graph=True)
            optimizer_t.step()
        # measure elapsed time

        losses.update(loss.item(), images.size(0))
        batch_time.update(time.time() - end)
        end = time.time()
        if i % args.print_freq == 0:
            progress.display(i)
    return losses.avg


def validate(val_loader, model, model_t, criterion, args):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    top1_t = AverageMeter('Acc_t@1', ':6.2f')
    top5_t = AverageMeter('Acc_t@5', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time, losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()
    model_t.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            output = model(images)
            output_t = model_t(images)
            loss = criterion(output, target)
            loss_t = criterion(output_t, target)

            # measure accuracy and record loss
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            acc1_t, acc5_t = accuracy(output_t, target, topk=(1, 5))
            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

            top1_t.update(acc1_t[0], images.size(0))
            top5_t.update(acc5_t[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                progress.display(i)

        # TODO: this should also be done with the ProgressMeter
        print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))
        # print(' * Acc_t@1 {} Acc_t@5 {}'
        #       .format(top1_t.avg, top5_t.avg))

    return top1.avg, top5.avg, top1_t.avg, top5_t.avg, losses.avg
