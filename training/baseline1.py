from common.utils import AverageMeter, ProgressMeter, accuracy
import torch
import time
# avg logits
# input: Tensor[3, 128, 10]
def avg_logits(te_scores_Tensor):
#     print(te_scores_Tensor.size())
    mean_Tensor = torch.mean(te_scores_Tensor, dim=1)
#     print(mean_Tensor)
    return mean_Tensor

def train(train_loader, model, model_t,loss_fn, fitnet_criterion, optimizer, epoch, args):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix="Epoch: [{}/{}]\t".format(epoch, args.epochs))

    # switch to train mode
    model.train()

    end = time.time()
    for i, (images, target) in enumerate(train_loader):
        #  print("start training")
        # measure data loading time
        fit_loss = 0
        data_time.update(time.time() - end)

        if args.gpu is not None:
            images = images.cuda(args.gpu, non_blocking=True)
        target = target.cuda(args.gpu, non_blocking=True)
        batch_size = images.shape[0]

        # compute output
        b1, b2, b3, b4, pool, output = model(images, extract=True)  # out_feat: 16, 32, 64, 64, -
        st_maps = [b1, b2, b3, b4]

        te_scores_list = []
        hint_maps = []
        t_b1s, t_b2s, t_b3s, t_b4s, t_pools = [], [], [], [], []
        for teacher in model_t:
            with torch.no_grad():
                t_b1, t_b2, t_b3, t_b4, t_pool, t_output = teacher(images, extract=True)
          #  hint_map = [t_b1, t_b2, t_b3, t_b4, t_pool]
            t_b1s.append(t_b1)
            t_b2s.append(t_b2)
            t_b3s.append(t_b3)
            t_b4s.append(t_b4)
            t_pools.append(t_pool)
            te_scores_list.append(t_output)
        te_scores_Tensor = torch.stack(te_scores_list, dim=1)  # size: [128, 3, 10]
        mean_logits = avg_logits(te_scores_Tensor)

        t_b1_tensor = torch.stack(t_b1s, dim=1)
        t_b2_tensor = torch.stack(t_b2s, dim=1)
        t_b3_tensor = torch.stack(t_b3s, dim=1)
        t_b4_tensor = torch.stack(t_b4s, dim=1)
        t_pool_tensor = torch.stack(t_pools, dim=1)

        mean_t_b1 = avg_logits(t_b1_tensor)
        mean_t_b2 = avg_logits(t_b2_tensor)
        mean_t_b3 = avg_logits(t_b3_tensor)
        mean_t_b4 = avg_logits(t_b4_tensor)
        mean_t_pool = avg_logits(t_pool_tensor)

        # hint_maps_avg = torch.stack(hint_maps, dim=1)
        # print(hint_maps_avg.shape)
        hint_maps_avg = [mean_t_b1, mean_t_b2, mean_t_b3, mean_t_b4]
       # print(mean_t_b1.shape)
        feat_num = len(hint_maps)
        fit_loss = 0
        for j, fitnet in enumerate(fitnet_criterion):
            fit_loss += fitnet(st_maps[j], hint_maps_avg[j]) / 2 ** (feat_num - j - 1)
        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss = loss_fn(output, target, mean_logits, T=args.T, alpha=args.alpha_kd)
        loss = loss + fit_loss/batch_size
        loss.backward()
        optimizer.step()

        # measure elapsed time

        losses.update(loss.item(), images.size(0))
        batch_time.update(time.time() - end)
        end = time.time()
        if i % args.print_freq == 0:
            progress.display(i)
    return losses.avg


def validate(val_loader, model, criterion, args):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time, losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            output = model(images)
            loss = criterion(output, target)

            # measure accuracy and record loss
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                progress.display(i)

        # TODO: this should also be done with the ProgressMeter
        print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))

    return top1.avg, top5.avg, losses.avg
