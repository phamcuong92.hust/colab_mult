python main_mult_origin_kd.py -t quantres18_32_42_mult \
      -a quantres18_2_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 256 \
      --type_data CIFAR10 \
      --ac ./checkpoints/quantres18_32_42_mult/model_best_CIFAR10.pth.tar \
      --lr 0.1 --lra 0.01 --lrc 0.005 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb


python main_mult_origin_kd.py -t quantres18_32_4_mult \
      -a quantres18_2_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 256 \
      --type_data CIFAR10 \
      --ac ./checkpoints/quantres18_32_4_mult/model_best_CIFAR10.pth.tar \
      --lr 0.1 --lra 0.01 --lrc 0.005 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb


python main_mult_origin_kd.py -t quantres18_42_mult \
      -a quantres18_2_mult \
      --epochs 100 --step-epoch 30 -j 4 -b 256 \
      --type_data CIFAR10 \
      --ac ./checkpoints/quantres18_42_mult/model_best_CIFAR10.pth.tar \
      --lr 0.1 --lra 0.01 --lrc 0.005 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 0 --use_wandb