import torch
import torch.nn.functional as F
import torch.nn as nn


def distillation(y, labels, teacher_scores, T, alpha):
    return F.kl_div(F.log_softmax(y / T, dim=1), F.softmax(teacher_scores / T, dim=1), reduction='batchmean') * (
            T * T) * alpha + F.cross_entropy(y, labels) * (1. - alpha)


class AttentionTransfer(nn.Module):
    def forward(self, student, teacher):
        s_attention = F.normalize(student.pow(2).mean(1).view(student.size(0), -1))

        with torch.no_grad():
            t_attention = F.normalize(teacher.pow(2).mean(1).view(teacher.size(0), -1))

        return (s_attention - t_attention).pow(2).mean()


class FitNet(nn.Module):
    def __init__(self, in_feature, out_feature):
        super(FitNet, self).__init__()
        self.in_feature = in_feature
        self.out_feature = out_feature

        self.transform = nn.Conv2d(in_feature, out_feature, 1, bias=False)
        self.transform.weight.data.uniform_(-0.005, 0.005)

    def forward(self, student, teacher):
        if student.dim() == 2:
            student = student.unsqueeze(2).unsqueeze(3)
            teacher = teacher.unsqueeze(2).unsqueeze(3)
        student = F.normalize(student)
        teacher = F.normalize(teacher)

        return (self.transform(student) - teacher).pow(2).mean()
