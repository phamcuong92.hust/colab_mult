python main_single.py -a quantmobilenet_32_mult \
              --epochs 100 \
              -j 4 -b 64 \
              --type_data CIFAR100 \
              --image_size 224 \
              --lr_decay_ratio 0.2 \
              --milestones 30 60 \
              --gamma 0.1 \
              --lr 0.02 --wd 4e-5 --gpu 1 --use_wandb


python main_single.py -a quantmobilenet_2_mult \
              --epochs 100 \
              -j 4 -b 256 \
              --type_data CIFAR100 \
              --image_size 224 \
              --lr_decay_ratio 0.2 \
              --milestones 30 60 \
              --gamma 0.1 \
              --lr 0.02 --wd 4e-5 --gpu 0 --use_wandb

python main_single.py -a quantmobilenet_32_mult \
              --epochs 100 \
              -j 4 -b 64 \
              --type_data CIFAR100 \
              --image_size 224 \
              --lr_decay_ratio 0.2 \
              --milestones 30 60 \
              --gamma 0.1 \
              --lr 0.02 --wd 4e-5 --gpu 1 --use_wandb