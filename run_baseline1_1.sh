python main_baseline1_1.py \
      -t1 quantres18_32_mult -t2 quantres18_4_mult \
      -a quantres18_2_mult \
      --epochs 150 --step-epoch 50 -j 4 -b 128 \
      --type_data CIFAR100 \
      --ac1 ./checkpoints/quantres18_32_mult/model_best_CIFAR100.pth.tar \
      --ac2 ./checkpoints/quantres18_4_mult/model_best_CIFAR100.pth.tar \
      --lr 0.1 --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 \
      --gpu 1 --use_wandb


python main_mult_fitnet.py \
      -t quantres18_32_4_mult -a quantres18_2_mult \
      --epochs 200 --step-epoch 50 -j 4 -b 128 --type_data CIFAR100 \
      --ac ./checkpoints/quantres18_32_4_mult/model_best_CIFAR100.pth.tar \
      --lr 0.1 --lra 0.001 --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 --gpu 1 --use_wandb

#python main_baseline1_1.py \
#      -t1 quantres18_32_mult -t2 quantres18_4_mult \
#      -a quantres18_2_mult \
#      --epochs 150 --step-epoch 50 -j 4 -b 128 \
#      --type_data CIFAR100 \
#      --ac1 ./checkpoints/quantres18_32_mult/model_best_CIFAR100.pth.tar \
#      --ac2 ./checkpoints/quantres18_4_mult/model_best_CIFAR100.pth.tar \
#      --lr 0.1 --wd 1e-4 --T 5 --alpha_kd 0.4 \
#      --gpu 1 --use_wandb


python main_baseline1_1.py \
      -t1 quantres18_8_mult -t2 quantres18_6_mult -t3 quantres18_4_mult\
      -a quantres18_2_mult \
      --epochs 150 --step-epoch 50 -j 4 -b 128 \
      --type_data CIFAR100 \
      --ac1 ./checkpoints/quantres18_8_mult/model_best_CIFAR100.pth.tar \
      --ac2 ./checkpoints/quantres18_6_mult/model_best_CIFAR100.pth.tar \
      --ac3 ./checkpoints/quantres18_4_mult/model_best_CIFAR100.pth.tar \
      --lr 0.1 --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 \
      --gpu 0 --use_wandb