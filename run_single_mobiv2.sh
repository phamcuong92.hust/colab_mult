
python main_single.py -a quantmobilenetv2_2_mult \
              --epochs 100 \
              -j 4 -b 64 \
              --type_data CIFAR100 \
              --image_size 224 \
              --lr_decay_ratio 0.2 \
              --milestones 30 60 \
              --gamma 0.1 \
              --lr 0.02 --wd 4e-5 --gpu 1 --use_wandb


python main_single.py -a quantmobilenetv2_32_mult \
              --epochs 100 \
              -j 4 -b 64 \
              --type_data CIFAR100 \
              --image_size 224 \
              --lr_decay_ratio 0.2 \
              --milestones 30 60 \
              --gamma 0.1 \
              --lr 0.02 --wd 4e-5 --gpu 1 --use_wandb

python main_single.py -a quantmobilenetv2_4_mult \
              --epochs 100 \
              -j 4 -b 64 \
              --type_data CIFAR100 \
              --image_size 224 \
              --lr_decay_ratio 0.2 \
              --milestones 30 60 \
              --gamma 0.1 \
              --lr 0.02 --wd 4e-5 --gpu 1 --use_wandb


#python main_single.py -a quantmobilenetv2_32_mult \
#              --epochs 200 \
#              -j 4 -b 2048 \
#              --type_data CIFAR10 \
#              --image_size 32 \
#              --lr_decay_ratio 0.2 \
#              --lr 0.05 --wd 1e-4 --gpu 1 --use_wandb


#python main_single.py -a quantmobilenetv2_4_mult \
#              --epochs 240 --step-epoch 40 -j 4 -b 2048 \
#              --type_data CIFAR10 \
#              --image_size 32 \
#              --lr 0.05 --wd 4e-5 --gpu 1 --use_wandb
#
#python main_single.py -a quantmobilenetv2_2_mult \
#              --epochs 160 --step-epoch 40 -j 4 -b 2048 \
#              --type_data CIFAR10 \
#              --image_size 32 \
#              --lr 0.05 --wd 4e-5 --gpu 1 --use_wandb
#
#
#python main_single.py -a quantmobilenetv2_32_mult \
#              --epochs 100 \
#              -j 4 -b 256 \
#              --type_data CIFAR100 \
#              --image_size 32 \
#              --lr_decay_ratio 0.2 \
#              --milestones 30 60 \
#              --gamma 0.1 \
#              --lr 0.02 --wd 4e-5 --gpu 1 --use_wandb
#
#python main_single.py -a mobilenet_v2 \
#              --epochs 100 \
#              -j 4 -b 256 \
#              --type_data CIFAR100 \
#              --image_size 32 \
#              --lr_decay_ratio 0.2 \
#              --milestones 30 60 \
#              --gamma 0.1 \
#              --lr 0.02 --wd 4e-5 --gpu 1 --use_wandb