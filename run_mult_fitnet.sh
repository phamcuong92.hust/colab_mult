python main_mult_fitnet.py \
      -t quantres18_32_42_mult -a quantres18_2_mult \
      --epochs 150 --step-epoch 50 -j 4 -b 64 \
      --type_data CIFAR100 \
      --ac ./checkpoints/quantres18_32_42_mult/model_best_CIFAR100.pth.tar \
      --lr 0.1 --lra 0.001 --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 \
      --gpu 0 --use_wandb

python main_mult_fitnet.py \
      -t quantres18_32_84_mult -a quantres18_2_mult \
      --epochs 150 --step-epoch 50 -j 4 -b 64 \
      --type_data CIFAR100 \
      --ac ./checkpoints/quantres18_32_84_mult/model_best_CIFAR100.pth.tar \
      --lr 0.1 --lra 0.001 --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 \
      --gpu 0 --use_wandb

python main_mult_fitnet.py \
      -t quantres18_32_4_mult -a quantres18_2_mult \
      --epochs 150 --step-epoch 50 -j 4 -b 64 \
      --type_data CIFAR100 \
      --ac ./checkpoints/quantres18_32_4_mult/model_best_CIFAR100.pth.tar \
      --lr 0.1 --lra 0.001 --lr_t 0.01 --wd 1e-4 --T 5 --alpha_kd 0.4 \
      --gpu 0 --use_wandb