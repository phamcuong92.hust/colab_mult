# train quantrest2 using teacher 842, load pretrain teacher from pretrained imagenet, load pretrain quantres2 from quantres234

import argparse
import os
import random
import shutil
import time
import warnings
from PIL import Image
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
from common.utils import *
from metric.loss import FitNet, distillation
import socket
from datasets import dataset_loader as ds
from torch.optim.lr_scheduler import MultiStepLR

import models as models
# from distiller_zoo import DistillKL
# from training.mult_fitnet import train_one_epoch_KD, validate
# from models import distiller
import wandb

model_names = sorted(name for name in models.__dict__
                     if name.islower() and not name.startswith("__")
                     and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument(
    "--use_wandb", default=False, action="store_true", help="If true, do not use wandb for this run",
)

parser.add_argument(
    "--test_resnet_small", default=False, action="store_true", help="If true, use resnet small for this run",
)

parser.add_argument('--data', type=str, default="/datassd/common/imagenet/", help='Dataset folder')  # path to dataset
parser.add_argument('--arch-cfg', '--ac', default='', type=str, metavar='PATH',
                    help='path to architecture configuration')
parser.add_argument('-a', '--arch_s', metavar='ARCH', default='quantres18_2_mult',
                    help='model architecture: ' +
                         ' | '.join(model_names) +
                         ' (default: quantres18_2_mult)')
parser.add_argument('-t', '--arch_t', metavar='ARCH', default='quantres18_864_mult',
                    help='model architecture: ' +
                         ' | '.join(model_names) +
                         ' (default: quantres18_864_mult)')
parser.add_argument('-j', '--workers', default=16, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--image_size', default=224, type=int, metavar='N',
                    help='Size of image training')
parser.add_argument('--epochs', default=100, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--step-epoch', default=30, type=int, metavar='N',
                    help='number of epochs to decay learning rate')

parser.add_argument('--gamma', type=float, default=0.1)
parser.add_argument('--milestones', type=int, nargs='+', default=[15, 30, 40, 45])

parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('-b', '--batch-size', default=256, type=int,
                    metavar='N',
                    help='mini-batch size (default: 256), this is the total '
                         'batch size of all GPUs on the current node when '
                         'using Data Parallel or Distributed Data Parallel')
parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    metavar='LR', help='initial learning rate', dest='lr')

parser.add_argument('--lr_t', '--learning-rate-teacher', default=0.1, type=float,
                    metavar='LR', help='initial learning rate', dest='lr_t')
parser.add_argument('--lra', '--learning-rate-alpha', default=0.01, type=float,
                    metavar='LR', help='initial alpha learning rate')

parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--wd', '--weight-decay', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)',
                    dest='weight_decay')

parser.add_argument('--qwd', '--quant_weight_decay', default=1e-7, type=float,
                    metavar='W', help='quant weight decay (default: 1e-7)',
                    dest='quant_weight_decay')

parser.add_argument('-p', '--print-freq', default=100, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')

# parser.add_argument(
#     "--resume_", default=False, action="store_true", help="If true, automatic resume weight in folder for this run",
# )
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')

parser.add_argument('--seed', default=113, type=int,
                    help='seed for initializing training. ')
parser.add_argument('--gpu', default=None, type=int,
                    help='GPU id to use.')

parser.add_argument('--optim', default="SGD", type=str,
                    help='optimziation method.')
parser.add_argument("--path_save", default="checkpoint", type=str,
                    help="path of the checkpoint model")

parser.add_argument('--end_lr', default=1e-5, type=float,
                    help='final learning rate')
parser.add_argument('--multiprocessing', action='store_true',
                    help='Use multi-processing distributed training to launch '
                         'N processes per node, which has N GPUs. This is the '
                         'fastest way to use PyTorch for either single node or '
                         'multi node data parallel training')
parser.add_argument('--type_data', default='CIFAR100', type=str,
                    choices=['CIFAR10', 'CIFAR100', 'ImageNet'],
                    help='type of data to training')

# parameter for loss
parser.add_argument('--T', type=float, default=2.0, metavar='Temperature', help='Temperature for distillation')
parser.add_argument('--alpha_s', type=float, default=1)  # weight for kl loss
parser.add_argument('--beta_s', type=float, default=1)  # weight for feature-based loss
parser.add_argument('--gamma_s', type=float, default=1000)  # weight for feature-based loss
parser.add_argument('--alpha_t', type=float, default=1)  # weight for feature-based loss
parser.add_argument('--beta_t', type=float, default=1)  # weight for feature-based loss
parser.add_argument('--gamma_t', type=float, default=1000)  # weight for feature-based loss
parser.add_argument('--lambda_cot_init', type=float, default=1e-5)  # weight for feature-based loss
# parser.add_argument('--lambda_cot', type=float, default=1e-4)  # weight for feature-based loss
parser.add_argument('--at_ratio_init', type=float, default=1000)  # weight for feature-based loss
best_acc1 = 0
best_acc5 = 0

best_acc1_t = 0
best_acc5_t = 0
comp_name = socket.gethostname()


def main():
    args = parser.parse_args()
    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. '
                      'This will turn on the CUDNN deterministic setting, '
                      'which can slow down your training considerably! '
                      'You may see unexpected behavior when restarting '
                      'from checkpoints.')

    if args.gpu is not None:
        warnings.warn('You have chosen a specific GPU. This will completely '
                      'disable data parallelism.')

    ngpus_per_node = torch.cuda.device_count()

    # Simply call main_worker function
    main_worker(args.gpu, ngpus_per_node, args)


def main_worker(gpu, ngpus_per_node, args):
    global best_acc1
    global best_acc5
    global best_acc1_t
    global best_acc5_t
    if args.use_wandb:
        wandb.init(project="Mult fitnet online distillation alexnet cifar")
    args.gpu = gpu

    if args.gpu is not None:
        print("Use GPU: {} for training".format(args.gpu))

    # create model
    print("=> creating model '{}'".format(args.arch_s))
    if args.type_data == "ImageNet":
        t_net = models.__dict__[args.arch_t](num_classes=1000)
        s_net = models.__dict__[args.arch_s](num_classes=1000)
    elif args.type_data == "CIFAR100":
        t_net = models.__dict__[args.arch_t](num_classes=100)
        s_net = models.__dict__[args.arch_s](num_classes=100)
    elif args.type_data == "CIFAR10":
        t_net = models.__dict__[args.arch_t](num_classes=10)
        s_net = models.__dict__[args.arch_s](num_classes=10)

    # load weight student
 #   path = "checkpoints/quantres18_2_mult/KD_att_248_shared_from_scratch/teacher/model_best_ImageNet.pth.tar"
 #    path = "checkpoints/quantres18_2_mult/KD_att_248_shared_from_scratch/teacher/model_best_ImageNet.pth.tar"  # path to teacher mix 234, accuracy 69.78 accuracy 65.78
 #    checkpoints = torch.load(path, map_location=lambda storage, loc: storage)
 #    state_dict = checkpoints["state_dict"]
 #    s_net.load_state_dict(state_dict, strict=False)

    # put model to cuda
    if args.gpu is not None:
        torch.cuda.set_device(args.gpu)
        t_net = t_net.cuda(args.gpu)
        s_net = s_net.cuda(args.gpu)
    else:
        # DataParallel will divide and allocate batch_size to all available GPUs
        t_net = torch.nn.DataParallel(t_net).cuda()
        s_net = torch.nn.DataParallel(s_net).cuda()

    print('Teacher Net: ')
    print(t_net)
    print('Student Net: ')
    print(s_net)
    print('the number of teacher model parameters: {}'.format(sum([p.data.nelement() for p in t_net.parameters()])))
    print('the number of student model parameters: {}'.format(sum([p.data.nelement() for p in s_net.parameters()])))

    # define loss function (criterion) and optimizer
    criterion_CE = nn.CrossEntropyLoss().cuda()
    criterion_KL = nn.KLDivLoss(reduction='batchmean')

    optimizer_s = torch.optim.SGD(s_net.parameters(), args.lr,
                                  momentum=args.momentum, weight_decay=25e-6, nesterov=True)

    cudnn.benchmark = True

    params, alpha_params = [], []
    for name, param in t_net.named_parameters():
        if 'alpha' in name:
            alpha_params += [param]
        else:
            params += [param]
    optimizer_t = torch.optim.SGD(
        [{'params': params, 'lr': args.lr_t, 'weight_decay': args.weight_decay},
         {'params': alpha_params, 'lr': args.lra, 'weight_decay': 0}],
        args.lr, momentum=args.momentum, weight_decay=args.weight_decay, nesterov=True)

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            if args.gpu is None:
                checkpoint = torch.load(args.resume)
            else:
                # Map model to be loaded to specified single gpu.
                loc = 'cuda:{}'.format(args.gpu)
                checkpoint = torch.load(args.resume, map_location=loc)
            args.start_epoch = checkpoint['epoch']
            best_acc1 = checkpoint['best_acc1']
            if args.gpu is not None:
                # best_acc1 may be from a checkpoint from a different GPU
                best_acc1 = best_acc1.to(args.gpu)
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            #  arch_optimizer.load_state_dict(checkpoint['arch_optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    # Get dataloader
    if 'inception' in args.arch_s:
        #  crop_size, short_size = 299, 342
        crop_size, short_size = 299, 342
    elif args.image_size == 32:
        crop_size, short_size = 32, 32
    else:
        crop_size, short_size = 224, 256
    if 'alex' in args.arch_s:
        crop_size, short_size = 227, 256
    train_loader, val_loader, train_sampler = ds.get_dataset(ds.Dataset[args.type_data], args.data,
                                                             batch_size=args.batch_size,
                                                             num_workers=args.workers,
                                                             crop_size=crop_size, short_size=short_size,
                                                             distributed=False,
                                                             enable_auto_augmentation=False)

    if args.evaluate:
        validate(val_loader, s_net, t_net, criterion_CE, args)
        return
    # check acc teacher model
    #  _, _, acc1_t, acc5_t, _ = validate(val_loader, s_net, t_net, criterion_CE, args)
    #  print("accuracy teacher model {}. {}".format(acc1_t, acc5_t))
    best_epoch = args.start_epoch
    scheduler_s = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer_s, T_max=args.epochs, eta_min=1e-11)
    scheduler_t = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer_t, T_max=args.epochs, eta_min=1e-11)
    for epoch in range(args.start_epoch, args.epochs):

        #  lambda_warmup(epoch, args) # warmup for lambda_cot
        #   adjust_learning_rate(optimizer_s, epoch, args)
        #  adjust_learning_rate_t(optimizer_t, epoch, args) # comment for imagenet

        # train for one epoch
        print("train epoch")
        loss_train, loss_cross_kd = train_one_epoch_KD(train_loader, s_net, t_net, optimizer_s, optimizer_t, criterion_CE,
                                                  criterion_KL, epoch, args)

        # evaluate on validation set
        acc1, acc5, acc1_t, acc5_t, loss_val = validate(val_loader, s_net, t_net, criterion_CE, args)

        scheduler_s.step()
        scheduler_t.step()
        print("accuracy teacher model {}. {}".format(acc1_t, acc5_t))
        # remember best acc@1 and save checkpoint
        is_best = acc1 > best_acc1
        best_acc1 = max(acc1, best_acc1)

        is_best_t = acc1_t > best_acc1_t
        best_acc1_t = max(acc1_t, best_acc1_t)

        if is_best_t:
            best_acc5_t = acc5_t
            best_epoch_t = epoch
        if is_best:
            best_epoch = epoch
            best_acc5 = acc5

        print('[' + comp_name + time.strftime(" %m/%d %H:%M:%S] ")
              + ' * Best Acc@1 {:.3f} - Best Acc@5 {:.3f} - epoch {:.7f}\n'.format(best_acc1, best_acc5, best_epoch))

        print('[' + comp_name + time.strftime(" %m/%d %H:%M:%S] ")
              + ' * Best Acc_t@1 {:.3f} - Best Acc_t@5 {:.3f} - epoch {:.7f}\n'.format(best_acc1_t, best_acc5_t,
                                                                                       best_epoch_t))
        if epoch > 80:
            path_checkpoint = "checkpoints/quantres18_2_mult/KD_cross_mult_cifar"
            save_checkpoint(path_checkpoint, {
                'epoch': epoch + 1,
                'arch': args.arch_s,
                'state_dict': s_net.state_dict(),
                'best_acc1': best_acc1,
                'optimizer': optimizer_s.state_dict()
            }, is_best, args.type_data)

            path_checkpoint_t = "checkpoints/quantres18_2_mult/KD_cross_mult_cifar" + "/teacher"
            save_checkpoint(path_checkpoint_t, {
                'epoch': epoch + 1,
                'arch': args.arch_t,
                'state_dict': t_net.state_dict(),
                'best_acc1': best_acc1_t,
                'optimizer': optimizer_t.state_dict()
            }, is_best_t, args.type_data)
        metrics = {'train_loss': loss_train,
                   'loss_cross_kd': loss_cross_kd,
                   "val_loss": loss_val,
                   "val_acc1": acc1,
                   "val_acc5": acc5,
                   "val_acc1_t": acc1_t,
                   "val_acc5_t": acc5_t
                   }
        if args.use_wandb:
            wandb.log(metrics)
    print('Best Acc@1 {0} Best Acc@5 {1} @ epoch {2}'.format(best_acc1, best_acc5, best_epoch))


from common.utils import AverageMeter, ProgressMeter, accuracy
import torch
import time
import torch.nn.functional as F
import torch.nn as nn
import numpy as np

from torch.autograd import Variable
class KLLoss_t2(nn.Module):
    def __init__(self):

        super(KLLoss_t2, self).__init__()
    def forward(self, pred, label):
        # pred: 2D matrix (batch_size, num_classes)
        # label: 1D vector indicating class number
        T=2

        predict = F.log_softmax(pred/T,dim=1)
        target_data = F.softmax(label/T,dim=1)
        target_data =target_data+10**(-7)
        target = Variable(target_data.data.cuda(),requires_grad=False)
        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss


class KLLoss_t3(nn.Module):
    def __init__(self):

        super(KLLoss_t3, self).__init__()
    def forward(self, pred, label):
        # pred: 2D matrix (batch_size, num_classes)
        # label: 1D vector indicating class number
        T=3

        predict = F.log_softmax(pred/T,dim=1)
        target_data = F.softmax(label/T,dim=1)
        target_data =target_data+10**(-7)
        target = Variable(target_data.data.cuda(),requires_grad=False)
        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss


class KLLoss_lowtem(nn.Module):
    def __init__(self):

        super(KLLoss_lowtem, self).__init__()
    def forward(self, pred, label):
        # pred: 2D matrix (batch_size, num_classes)
        # label: 1D vector indicating class number
        T=1

        predict = F.log_softmax(pred/T,dim=1)
        target_data = F.softmax(label/T,dim=1)
        target_data =target_data+10**(-7)
        target = Variable(target_data.data.cuda(),requires_grad=False)
        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss


def sigmoid_rampup(current, rampup_length):
    """Exponential rampup from https://arxiv.org/abs/1610.02242"""
    if rampup_length == 0:
        return 1.0
    else:
        current = np.clip(current, 0.0, rampup_length)
        phase = 1.0 - current / rampup_length
        return float(np.exp(-5.0 * phase * phase))

def train_one_epoch_KD(train_loader, s_net, t_net, optimizer_s, optimizer_t, criterion_CE, criterion_KL, epoch, args):
    """
    Train colaborative model for one epoch
    :param model_list: list of model including teacher and student
    :param optimizer_list: list of optimizer
    :param train_loader: train loader dataset
    :param epoch: index of epoch
    :param args: parameters
    :return: loss and accuracy of all model
    """
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    losses_att = AverageMeter('Lossdistill', ':.4e')
    losses_cross_kd = AverageMeter('losses_cross_kd', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix="Epoch: [{}/{}]\t".format(epoch, args.epochs))

    # switch to train mode
    s_net.train()
    t_net.train()
    criterion_KL1 = KLLoss_t3()
    criterion_KL2 = KLLoss_lowtem()
    end = time.time()
    consistency_weight = sigmoid_rampup(epoch, 10)
    for i, (images, targets) in enumerate(train_loader):
        #  print("start training")
        # measure data loading time
        data_time.update(time.time() - end)
        if args.gpu is not None:
            images = images.cuda(args.gpu)
            targets = targets.cuda(args.gpu)
        else:
            targets = targets.cuda()
        batch_size = images.shape[0]

        # compute output
        t_feats, t_out = t_net(images, extract=True)
        soft_label_t = F.softmax(t_out, dim=1)
        s_feats, s_out = s_net(images, extract=True)
        feat_num = len(t_feats)
        loss_att_s = 0


        # for j in range(feat_num):
        #     t_feats_q = HWGQ(2)(t_feats[j])  # quantize to 2 bit
        #     attention_map = s_feats[j].pow(2).mean(1, keepdim=True)
        #     target_attenion_map = t_feats_q.pow(2).mean(1, keepdim=True)
        #
        #     loss_att_s += attention_loss(attention_map, target_attenion_map.detach())

        loss_CE_s = criterion_CE(s_out, targets)
        # compute gradient and do SGD step
        # kd_loss_s = (args.T**2)*criterion_KL(F.log_softmax(s_out/args.T, dim=1), F.softmax(t_out.detach()/args.T, dim=1))/s_out.shape[0]

       # loss_s = args.alpha_s * loss_CE_s \
        #         + args.beta_s * kd_loss_s + args.gamma_s * loss_att_s
        soft_teacher = min_logit(s_out, t_out, targets)

        kd_loss_s = criterion_KL1(s_out, soft_teacher)
        kd_loss_t = criterion_KL2(t_out, soft_teacher)
        loss_CE_t = criterion_CE(t_out, targets)
     #   loss_t = args.alpha_t * loss_CE_t
        loss_total = loss_CE_t + loss_CE_s + args.beta_s*(kd_loss_t + kd_loss_s)
        optimizer_s.zero_grad()
        optimizer_t.zero_grad()
        loss_total.backward()
        optimizer_s.step()
        optimizer_t.step()
        # update teacher
        # loss_CE_t = criterion_CE(t_out, targets)
        # loss_t = args.alpha_t*loss_CE_t \
        #         + args.beta_t * kd_loss_t
        # optimizer_t.zero_grad()
        # loss_t.backward()
        # optimizer_t.step()

        # measure accuracy and record loss
        acc1, acc5 = accuracy(s_out, targets, topk=(1, 5))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))
        # losses.update(loss_s.item(), images.size(0))
        losses.update(loss_CE_s.item(), images.size(0))
      #  losses_att.update(loss_att_s.sum().item(), images.size(0))
        losses_cross_kd.update(kd_loss_s.sum().item(), images.size(0))
        batch_time.update(time.time() - end)
        end = time.time()
        if i % args.print_freq == 0:
            progress.display(i)
    return losses.avg, losses_cross_kd.avg


def validate(val_loader, model, model_t, criterion, args):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    top1_t = AverageMeter('Acc_t@1', ':6.2f')
    top5_t = AverageMeter('Acc_t@5', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time, losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()
    model_t.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target) in enumerate(val_loader):
            if args.gpu is not None:
                images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)

            # compute output
            output = model(images)
            output_t = model_t(images)
            loss = criterion(output, target)
            loss_t = criterion(output_t, target)

            # measure accuracy and record loss
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            acc1_t, acc5_t = accuracy(output_t, target, topk=(1, 5))
            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

            top1_t.update(acc1_t[0], images.size(0))
            top5_t.update(acc5_t[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                progress.display(i)

        # TODO: this should also be done with the ProgressMeter
        print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))
        print(' * Acc_t@1 {} Acc_t@5 {}'
              .format(top1_t.avg, top5_t.avg))
    # if args.gpu is not None:
    #     model_t.show_params()
    # else:
    #     model_t.module.show_params()
    #
    return top1.avg, top5.avg, top1_t.avg, top5_t.avg, losses.avg


# overhaul distillation
import torch
import torch.nn as nn
import torch.nn.functional as F
from scipy.stats import norm
import scipy

import math
from modules_q.quant_mult import _gauss_quantize


def distillation_loss(source, target):
    loss = torch.nn.functional.mse_loss(source, target, reduction="none")
    return loss.sum()


def build_feature_connector(t_channel, s_channel):
    C = [nn.Conv2d(s_channel, t_channel, kernel_size=1, stride=1, padding=0, bias=False),
         nn.BatchNorm2d(t_channel)]

    for m in C:
        if isinstance(m, nn.Conv2d):
            n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            m.weight.data.normal_(0, math.sqrt(2. / n))
        elif isinstance(m, nn.BatchNorm2d):
            m.weight.data.fill_(1)
            m.bias.data.zero_()
    return nn.Sequential(*C)


class Distiller(nn.Module):
    def __init__(self, t_net, s_net):
        super(Distiller, self).__init__()

        t_channels = t_net.get_channel_num()
        s_channels = s_net.get_channel_num()

        self.t_net = t_net
        self.s_net = s_net

    def forward(self, x):
        t_feats, t_out = self.t_net(x, extract=True)
        s_feats, s_out = self.s_net(x, extract=True)
        feat_num = len(t_feats)

        loss_distill = 0
        loss_at = 0

        for i in range(feat_num):
            t_feats_q = _gauss_quantize.apply(t_feats[i], 0.996, 2)  # quantize to 2 bit
            attention_map = s_feats[i].pow(2).mean(1, keepdim=True)
            target_attenion_map = t_feats_q.pow(2).mean(1, keepdim=True)

            loss_at += attention_loss(attention_map, target_attenion_map)

        return s_out, t_out, loss_at


def attention_loss(x, y):
    x = F.normalize(x.view(x.size(0), -1))
    y = F.normalize(y.view(y.size(0), -1))
    return (x - y).pow(2).mean()


# overhaul distillation
import torch
import torch.nn as nn
import torch.nn.functional as F
from scipy.stats import norm
import scipy

import math
from modules_q.quant_mult import _gauss_quantize, HWGQ


def cross_entropy_loss_with_soft_target(pred, soft_target):
    logsoftmax = nn.LogSoftmax()
    return torch.mean(torch.sum(- soft_target * logsoftmax(pred), dim=1))

def min_logit(s_out, t_out, targets):
    output = torch.zeros_like(s_out)
    out1 = torch.zeros_like(s_out)
    out2 = torch.zeros_like(s_out)
    for i in range(len(s_out)):
        index = targets[i].item()
        out1[i] = s_out[i] - s_out[i][index]
        out2[i] = t_out[i] - t_out[i][index]
        output = torch.min(out1, out2)
    return output



if __name__ == "__main__":
    main()